#pragma once

#include "state/statebase.hpp"
#include "state/common/common.hpp"
#include "state/wait/wait.hpp"

namespace midi2usb {

class App {
 public:
  App(ReportBuffers* reports)
      : state_{APPSTATE::WAIT},
        ids_{0, 0},
        wait_{&state_, &ids_, reports},
        common_{&state_, &ids_, reports} {}

  void Update(MidiW* midiw) {
    switch (state_) {
      case APPSTATE::WAIT:
        return wait_.Update(midiw);
      case APPSTATE::COMMON:
        return common_.Update(midiw);
      case APPSTATE::PADKONTROL:
        return common_.Update(midiw);
      default:
        break;
    }
  }

  void Input(MidiRW* midirw) {
    switch (state_) {
      case APPSTATE::WAIT:
        return wait_.Input(midirw);
      case APPSTATE::COMMON:
        return common_.Input(midirw);
      case APPSTATE::PADKONTROL:
        return common_.Input(midirw);
      default:
        break;
    }
  }

  void WriteReport() {
    switch (state_) {
      case APPSTATE::WAIT:
        return wait_.WriteReport();
      case APPSTATE::COMMON:
        return common_.WriteReport();
      case APPSTATE::PADKONTROL:
        return common_.WriteReport();
      default:
        break;
    }
  }

 private:
  APPSTATE state_;
  IDs ids_;
  WaitState wait_;
  CommonState common_;
};

};  // namespace midi2usb
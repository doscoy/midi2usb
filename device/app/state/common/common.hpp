#pragma once

#include "../statebase.hpp"
#include "table.hpp"

namespace midi2usb {  // declaration

class CommonState : public BaseState {
  enum STATE { STANBY, RUNNING };

 public:
  using BaseState::BaseState;

  void Update(MidiW* midiw) override;

 protected:
  void InputImpl(MidiRW* midirw) override;
  void CleanUp() override;

 private:
  void HandlingNote(uint8_t ch, uint8_t note, uint8_t vel, bool on);
  void ReadMouseData(uint8_t note, uint8_t vel, bool on);
  void ReadGamePadData(uint8_t note, uint8_t vel, bool on);

  STATE state_{STATE::STANBY};
};

}  // namespace midi2usb

namespace midi2usb {  // implementation

void CommonState::Update(MidiW* midiw) {
  if (state_ == STATE::STANBY) {
    const uint8_t recvfilter[]{
        0xF0, kLocalSysExID, RECEIVECABLE,
        commontable::kReceiveFilter.Get(ids_->vid, ids_->pid), 0xF7};

    MIDI2USBDEBUGPRINTF("filter: %02x %02x %02x %02x %02x", recvfilter[0],
                        recvfilter[1], recvfilter[2], recvfilter[3],
                        recvfilter[4])

    midiw->Send(recvfilter, sizeof(recvfilter));
    state_ = STATE::RUNNING;
  }
}

void CommonState::InputImpl(MidiRW* midirw) {
  if (state_ != STATE::RUNNING) return;

  switch (midirw->GetType()) {
    case midi::MidiType::NoteOn:
      return commontable::kNoteTable.Call(midirw->GetCh(), &makers_, true,
                                          midirw->GetData1(),
                                          midirw->GetData2());

    case midi::MidiType::NoteOff:
      return commontable::kNoteTable.Call(midirw->GetCh(), &makers_, false,
                                          midirw->GetData1(),
                                          midirw->GetData2());

    case midi::MidiType::ControlChange:
      return commontable::kCCTable.Call(midirw->GetCh(), &makers_,
                                        midirw->GetData1(), midirw->GetData2());
    
    case midi::MidiType::ProgramChange:
      return commontable::kProgTable.Call(midirw->GetCh(), &makers_,
                                          midirw->GetData1());
    
    case midi::MidiType::Start:
      makers_.consumer.Push(0x00B0); // play
      makers_.consumer.SetTap();
      break;
    
    case midi::MidiType::Continue:
      makers_.consumer.Push(0x00CD); // play/pause
      makers_.consumer.SetTap();
      break;

    case midi::MidiType::Stop:
      makers_.consumer.Push(0x00B7); // stop
      makers_.consumer.SetTap();
      break;

    default:
      break;
  }
}

void CommonState::CleanUp() {
  MIDI2USBDEBUGPRINT("cleanup common")
  state_ = STATE::STANBY;
}

}  // namespace midi2usb
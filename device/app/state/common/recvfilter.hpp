#pragma once

#include <algorithm>
#include <iterator>

#include "../../../debug.hpp"

namespace midi2usb {

namespace detail {

struct DefaultSetting {
  bool use;
  uint8_t num;
};

struct DeviceSetting {
  uint16_t vid;
  uint16_t pid;
  bool use;
  uint8_t num;
};

using DefaultSettingMaker = detail::DefaultSetting (*)(void);
using DeviceSettingMaker = detail::DeviceSetting (*)(void);

}  // namespace detail

template <detail::DefaultSettingMaker Default,
          detail::DeviceSettingMaker... Device>
class ReceiveFilter {
 public:
  constexpr ReceiveFilter()
      : default_{GenMsg(Default())}, table_{GenItem(Device())...} {}

  constexpr uint8_t Get(uint16_t vid, uint16_t pid) const {
    const auto id{GenID(vid, pid)};
    const auto fnc{[id](const auto& item) { return item.id == id; }};
    const auto ptr{std::find_if(std::begin(table_), std::end(table_), fnc)};
    return ptr != std::end(table_) ? ptr->msg : default_;
  }

 private:
  struct Item {
    uint32_t id;
    uint8_t msg;
  };

  static constexpr uint32_t GenID(uint16_t vid, uint16_t pid) {
    return (vid << 16u) | pid;
  }

  static constexpr uint32_t GenID(const detail::DeviceSetting& device_setting) {
    return GenID(device_setting.vid, device_setting.pid);
  }

  static constexpr uint8_t GenMsg(bool use, uint8_t num) {
    return (use ? 0x10u : 0x00u) | (num & 0x0Fu);
  }

  static constexpr uint8_t GenMsg(
      const detail::DefaultSetting& default_setting) {
    return GenMsg(default_setting.use, default_setting.num);
  }

  static constexpr uint8_t GenMsg(const detail::DeviceSetting& device_setting) {
    return GenMsg(device_setting.use, device_setting.num);
  }

  static constexpr Item GenItem(const detail::DeviceSetting& setting) {
    return Item{GenID(setting), GenMsg(setting)};
  }

  const uint8_t default_;
  const Item table_[sizeof...(Device)];
};

template <detail::DefaultSettingMaker Default>
class ReceiveFilter<Default> {
 public:
  constexpr ReceiveFilter() : default_{GenMsg(Default())} {}

  constexpr uint8_t Get(uint16_t, uint16_t) const { return default_; }

 private:
  static constexpr uint32_t GenID(uint16_t vid, uint16_t pid) {
    return (vid << 16u) | pid;
  }

  static constexpr uint8_t GenMsg(bool use, uint8_t num) {
    return (use ? 0x10u : 0x00u) | (num & 0x0Fu);
  }

  static constexpr uint8_t GenMsg(
      const detail::DefaultSetting& default_setting) {
    return GenMsg(default_setting.use, default_setting.num);
  }

  const uint8_t default_;
};

template <bool Use, uint8_t Cable>
constexpr detail::DefaultSetting DefaultFilter() {
  return detail::DefaultSetting{Use, Cable};
}

template <uint16_t VID, uint16_t PID, bool Use, uint8_t Cable>
constexpr detail::DeviceSetting DeviceFilter() {
  return detail::DeviceSetting{VID, PID, Use, Cable};
}

}  // namespace midi2usb
#pragma once

#include "../tablebase.hpp"
#include "recvfilter.hpp"

namespace midi2usb {

namespace commontable {

inline static constexpr ReceiveFilter<
    DefaultFilter<false, 0>,
    DeviceFilter<0x1235, 0x0113, true, 1>  // launchpad mini mk3
    >
    kReceiveFilter{};

uint8_t MouseDataModify(uint8_t data1, uint8_t data2, size_t tablesize);
uint8_t GamePadDataModify(uint8_t data1, uint8_t data2, size_t tablesize);

inline static constexpr char kStr[]{"Hello World!!"};

inline static constexpr auto kCmdList{MakeStrList(
    /*   0 */ "pwd",
    /*   1 */ "cd",
    /*   2 */ "mkdir",
    /*   3 */ "rm",
    /*   4 */ "cp",
    /*   5 */ "mv",
    /*   6 */ "ls",
    /*   7 */ "cd",
    /*   8 */ "pushd",
    /*   9 */ "popd",
    /*  10 */ "cat",
    /*  11 */ "rm",
    /*  12 */ "cp",
    /*  13 */ "mv",
    /*  14 */ "touch",
    /*  15 */ "file",
    /*  16 */ "find",
    /*  17 */ "locate",
    /*  18 */ "more",
    /*  19 */ "less",
    /*  20 */ "lv",
    /*  21 */ "head",
    /*  22 */ "tail",
    /*  23 */ "grep",
    /*  24 */ "sed",
    /*  25 */ "wc",
    /*  26 */ "diff",
    /*  27 */ "cut",
    /*  28 */ "echo",
    /*  29 */ "uniq",
    /*  30 */ "sort",
    /*  31 */ "nkf",
    /*  32 */ "dos2unix",
    /*  33 */ "unix2dos",
    /*  34 */ "history",
    /*  35 */ "ps",
    /*  36 */ "jobs",
    /*  37 */ "kill",
    /*  38 */ "lp",
    /*  39 */ "lpstat",
    /*  40 */ "cancel",
    /*  41 */ "yppasswd",
    /*  42 */ "chmod",
    /*  43 */ "xlock",
    /*  44 */ "last",
    /*  45 */ "du",
    /*  46 */ "df",
    /*  47 */ "ln",
    /*  48 */ "alias",
    /*  49 */ "unalias",
    /*  50 */ "man",
    /*  51 */ "which",
    /*  52 */ "tar",
    /*  53 */ "gunzip",
    /*  54 */ "p7zip",
    /*  55 */ "ip",
    /*  56 */ "telnet",
    /*  57 */ "ssh",
    /*  58 */ "scp",
    /*  59 */ "sftp",
    /*  60 */ "curl",
    /*  61 */ "nmcli",
    /*  62 */ "su",
    /*  63 */ "shutdown",
    /*  64 */ "reboot",
    /*  65 */ "hostname",
    /*  66 */ "groups",
    /*  67 */ "chown",
    /*  68 */ "chgrp",
    /*  69 */ "useradd",
    /*  70 */ "userdel",
    /*  71 */ "groupadd",
    /*  72 */ "groupdel",
    /*  73 */ "passwd",
    /*  74 */ "who",
    /*  75 */ "whoami",
    /*  76 */ "set",
    /*  77 */ "printenv",
    /*  78 */ "export",
    /*  79 */ "unset",
    /*  80 */ "source",
    /*  81 */ "watch",
    /*  82 */ "chmail",
    /*  83 */ "stty",
    /*  84 */ "date")};

inline static constexpr auto kTwitchEmoteList{MakeStrList(
    /*   0 */ "8-)",
    /*   1 */ ":-(",
    /*   2 */ ":-)",
    /*   3 */ ":-/",
    /*   4 */ ":-D",
    /*   5 */ ":-O",
    /*   6 */ ":-P",
    /*   7 */ ":-|",
    /*   8 */ ";-)",
    /*   9 */ ";-P",
    /*  10 */ ">(",
    /*  11 */ "O_O",
    /*  12 */ "R-)",
    /*  13 */ "<3",
    /*  14 */ "4Head",
    /*  15 */ "ANELE",
    /*  16 */ "ArgieB8",
    /*  17 */ "ArsonNoSexy",
    /*  18 */ "AsexualPride",
    /*  19 */ "AsianGlow",
    /*  20 */ "BCWarrior",
    /*  21 */ "BOP",
    /*  22 */ "BabyRage",
    /*  23 */ "BatChest",
    /*  24 */ "BegWan",
    /*  25 */ "BibleThump",
    /*  26 */ "BigBrother",
    /*  27 */ "BigPhish",
    /*  28 */ "BisexualPride",
    /*  29 */ "BlackLivesMatter",
    /*  30 */ "BlargNaut",
    /*  31 */ "BloodTrail",
    /*  32 */ "BrainSlug",
    /*  33 */ "BrokeBack",
    /*  34 */ "BuddhaBar",
    /*  35 */ "CaitlynS",
    /*  36 */ "CarlSmile",
    /*  37 */ "ChefFrank",
    /*  38 */ "CoolCat",
    /*  39 */ "CoolStoryBob",
    /*  40 */ "CorgiDerp",
    /*  41 */ "CrreamAwk",
    /*  42 */ "CurseLit",
    /*  43 */ "DAESuppy",
    /*  44 */ "DBstyle",
    /*  45 */ "DansGame",
    /*  46 */ "DarkKnight",
    /*  47 */ "DarkMode",
    /*  48 */ "DatSheffy",
    /*  49 */ "DendiFace",
    /*  50 */ "DogFace",
    /*  51 */ "DoritosChip",
    /*  52 */ "DxCat",
    /*  53 */ "EarthDay",
    /*  54 */ "EleGiggle",
    /*  55 */ "EntropyWins",
    /*  56 */ "ExtraLife",
    /*  57 */ "FBBlock",
    /*  58 */ "FBCatch",
    /*  59 */ "FBChallenge",
    /*  60 */ "FBPass",
    /*  61 */ "FBPenalty",
    /*  62 */ "FBRun",
    /*  63 */ "FBSpiral",
    /*  64 */ "FBtouchdown",
    /*  65 */ "FUNgineer",
    /*  66 */ "FailFish",
    /*  67 */ "FamilyMan",
    /*  68 */ "FootBall",
    /*  69 */ "FootGoal",
    /*  70 */ "FootYellow",
    /*  71 */ "FrankerZ",
    /*  72 */ "FreakinStinkin",
    /*  73 */ "FutureMan",
    /*  74 */ "GayPride",
    /*  75 */ "GenderFluidPride",
    /*  76 */ "GingerPower",
    /*  77 */ "GivePLZ",
    /*  78 */ "GlitchCat",
    /*  79 */ "GlitchLit",
    /*  80 */ "GlitchNRG",
    /*  81 */ "GrammarKing",
    /*  82 */ "GunRun",
    /*  83 */ "HSCheers",
    /*  84 */ "HSWP",
    /*  85 */ "HarleyWink",
    /*  86 */ "HassaanChop",
    /*  87 */ "HeyGuys",
    /*  88 */ "HolidayCookie",
    /*  89 */ "HolidayLog",
    /*  90 */ "HolidayPresent",
    /*  91 */ "HolidaySanta",
    /*  92 */ "HolidayTree",
    /*  93 */ "HotPokket",
    /*  94 */ "HungryPaimon",
    /*  95 */ "ImTyping",
    /*  96 */ "IntersexPride",
    /*  97 */ "InuyoFace",
    /*  98 */ "ItsBoshyTime",
    /*  99 */ "JKanStyle",
    /* 100 */ "Jebaited",
    /* 101 */ "Jebasted",
    /* 102 */ "JonCarnage",
    /* 103 */ "KAPOW",
    /* 104 */ "KEKHeim",
    /* 105 */ "Kappa",
    /* 106 */ "KappaClaus",
    /* 107 */ "KappaPride",
    /* 108 */ "KappaRoss",
    /* 109 */ "KappaWealth",
    /* 110 */ "Kappu",
    /* 111 */ "Keepo",
    /* 112 */ "KevinTurtle",
    /* 113 */ "Kippa",
    /* 114 */ "KomodoHype",
    /* 115 */ "KonCha",
    /* 116 */ "Kreygasm",
    /* 117 */ "LUL",
    /* 118 */ "LaundryBasket",
    /* 119 */ "LesbianPride",
    /* 120 */ "reserve",
    /* 121 */ "reserve",
    /* 122 */ "reserve",
    /* 123 */ "reserve",
    /* 124 */ "reserve",
    /* 125 */ "reserve",
    /* 126 */ "reserve",
    /* 127 */ "reserve")};

// clang-format off
inline static constexpr Note2::FunctionTable<
    /* ch 1 */ Note2::Keyboard<0x00>,
    /* ch 2 */ Note2::Keyboard<0x80>,
    /* ch 3 */ Note2::Keyboard<0x00, MODKEY::CTRL_L>,
    /* ch 4 */ Note2::Keyboard<0x00, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
    /* ch 5 */ Note2::Keyboard<0x00, MODKEY::CTRL_L, MODKEY::ALT_L>,
    /* ch 6 */ Note2::Keyboard<0x00, MODKEY::CTRL_L, MODKEY::GUI_L>,
    /* ch 7 */ Note2::Keyboard<0x00, MODKEY::ALT_L>,
    /* ch 8 */ Note2::Keyboard<0x00, MODKEY::ALT_L, MODKEY::SHIFT_L>,
    /* ch 9 */ Note2::Keyboard<0x00, MODKEY::ALT_L, MODKEY::GUI_L>,
    /* ch10 */ Note2::Keyboard<0x00, MODKEY::GUI_L>,
    /* ch11 */ Note2::Keyboard<0x00, MODKEY::GUI_L, MODKEY::SHIFT_L>,
    /* ch12 */ Note2::Keyboard<0x00, MODKEY::SHIFT_L>,
    /* ch13 */ Note2::Selector<
        /* mod */ MouseDataModify,
        /*  C  */ Note2::MouseMove<MOUSEMOVE::X_P>,
        /*  C# */ Note2::MouseMove<MOUSEMOVE::X_N>,
        /*  D  */ Note2::MouseMove<MOUSEMOVE::Y_P>,
        /*  D# */ Note2::MouseMove<MOUSEMOVE::Y_N>,
        /*  E  */ Note2::MouseMove<MOUSEMOVE::WHEEL_P>,
        /*  F  */ Note2::MouseMove<MOUSEMOVE::WHEEL_N>,
        /*  F# */ Note2::MouseMove<MOUSEMOVE::PAN_P>,
        /*  G  */ Note2::MouseMove<MOUSEMOVE::PAN_N>,
        /*  G# */ Blank,
        /*  A  */ Note2::MouseButton<MOUSEBUTTON::LEFT>,
        /*  A# */ Note2::MouseButton<MOUSEBUTTON::MIDDLE>,
        /*  B  */ Note2::MouseButton<MOUSEBUTTON::RIGHT>>,
    /* ch14 */ Note2::Selector<
        /* mod */ GamePadDataModify,
        /*  C  */ Note2::GamePadAxis<GAMEPADAXIS::X_P>,
        /*  C# */ Note2::GamePadAxis<GAMEPADAXIS::X_N>,
        /*  D  */ Note2::GamePadAxis<GAMEPADAXIS::Y_P>,
        /*  D# */ Note2::GamePadAxis<GAMEPADAXIS::Y_N>,
        /*  E  */ Note2::GamePadAxis<GAMEPADAXIS::Z_P>,
        /*  F  */ Note2::GamePadAxis<GAMEPADAXIS::Z_N>,
        /*  F# */ Note2::GamePadAxis<GAMEPADAXIS::RZ_P>,
        /*  G  */ Note2::GamePadAxis<GAMEPADAXIS::RZ_N>,
        /*  G# */ Note2::GamePadButton<GAMEPADBUTTON::NO1>,
        /*  A  */ Note2::GamePadButton<GAMEPADBUTTON::NO2>,
        /*  A# */ Note2::GamePadButton<GAMEPADBUTTON::NO3>,
        /*  B  */ Note2::GamePadButton<GAMEPADBUTTON::NO4>,
        /*  C  */ Note2::GamePadButton<GAMEPADBUTTON::NO5>,
        /*  C# */ Note2::GamePadButton<GAMEPADBUTTON::NO6>,
        /*  D  */ Note2::GamePadButton<GAMEPADBUTTON::NO7>,
        /*  D# */ Note2::GamePadButton<GAMEPADBUTTON::NO8>,
        /*  E  */ Note2::GamePadButton<GAMEPADBUTTON::NO9>,
        /*  F  */ Note2::GamePadButton<GAMEPADBUTTON::NO10>,
        /*  F# */ Note2::GamePadButton<GAMEPADBUTTON::NO11>,
        /*  G  */ Note2::GamePadButton<GAMEPADBUTTON::NO12>,
        /*  G# */ Note2::GamePadButton<GAMEPADBUTTON::NO13>,
        /*  A  */ Note2::GamePadButton<GAMEPADBUTTON::NO14>,
        /*  A# */ Note2::GamePadButton<GAMEPADBUTTON::NO15>,
        /*  B  */ Note2::GamePadButton<GAMEPADBUTTON::NO16>>,
    /* ch15 */ Note2::AppControll<0x00>,
    /* ch16 */ Note2::AppControll<0x80>>
    kNoteTable{};
// clang-format on

// clang-format off
inline static constexpr CC2::FunctionTable<
    /* ch 1 */ CC2::Keyboard<0x00>,
    /* ch 2 */ CC2::Keyboard<0x80>,
    /* ch 3 */ CC2::Keyboard<0x00, MODKEY::CTRL_L>,
    /* ch 4 */ CC2::Keyboard<0x00, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
    /* ch 5 */ CC2::Keyboard<0x00, MODKEY::CTRL_L, MODKEY::ALT_L>,
    /* ch 6 */ CC2::Keyboard<0x00, MODKEY::CTRL_L, MODKEY::GUI_L>,
    /* ch 7 */ CC2::Keyboard<0x00, MODKEY::ALT_L>,
    /* ch 8 */ CC2::Keyboard<0x00, MODKEY::ALT_L, MODKEY::SHIFT_L>,
    /* ch 9 */ CC2::Keyboard<0x00, MODKEY::ALT_L, MODKEY::GUI_L>,
    /* ch10 */ CC2::Keyboard<0x00, MODKEY::GUI_L>,
    /* ch11 */ CC2::Keyboard<0x00, MODKEY::GUI_L, MODKEY::SHIFT_L>,
    /* ch12 */ CC2::Keyboard<0x00, MODKEY::SHIFT_L>,
    /* ch13 */ CC2::Selector<
        /* mod */ NoModify,
        /*   0 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 1>>,
        /*   1 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 1>>,
        /*   2 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 1>>,
        /*   3 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 1>>,
        /*   4 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 1>>,
        /*   5 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 1>>,
        /*   6 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 1>>,
        /*   7 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 1>>,
        /*   8 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 1>>,
        /*   9 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 1>>,
        /*  10 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 1>>,
        /*  11 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 1>>,
        /*  12 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 1>>,
        /*  13 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 1>>,
        /*  14 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 1>>,
        /*  15 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 1>>,
        /*  16 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 1>>,
        /*  17 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 1>>,
        /*  18 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 1>>,
        /*  19 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 1>>,
        /*  20 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 1>>,
        /*  21 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 1>>,
        /*  22 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 1>>,
        /*  23 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 1>>,
        /*  24 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 1>>,
        /*  25 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 1>>,
        /*  26 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 1>>,
        /*  27 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 1>>,
        /*  28 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 1>>,
        /*  29 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 1>>,
        /*  30 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 1>>,
        /*  31 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 1>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 1>>,
        /*  32 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 2>>,
        /*  33 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 2>>,
        /*  34 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 2>>,
        /*  35 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 2>>,
        /*  36 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 2>>,
        /*  37 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 2>>,
        /*  38 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 2>>,
        /*  39 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 2>>,
        /*  40 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 2>>,
        /*  41 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 2>>,
        /*  42 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 2>>,
        /*  43 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 2>>,
        /*  44 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 2>>,
        /*  45 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 2>>,
        /*  46 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 2>>,
        /*  47 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 2>>,
        /*  48 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 2>>,
        /*  49 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 2>>,
        /*  50 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 2>>,
        /*  51 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 2>>,
        /*  52 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 2>>,
        /*  53 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 2>>,
        /*  54 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 2>>,
        /*  55 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 2>>,
        /*  56 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 2>>,
        /*  57 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 2>>,
        /*  58 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 2>>,
        /*  59 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 2>>,
        /*  60 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 2>>,
        /*  61 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 2>>,
        /*  62 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 2>>,
        /*  63 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 2>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 2>>,
        /*  64 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 4>>,
        /*  65 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 4>>,
        /*  66 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 4>>,
        /*  67 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 4>>,
        /*  68 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 4>>,
        /*  69 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 4>>,
        /*  70 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 4>>,
        /*  71 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 4>>,
        /*  72 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 4>>,
        /*  73 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 4>>,
        /*  74 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 4>>,
        /*  75 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 4>>,
        /*  76 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 4>>,
        /*  77 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 4>>,
        /*  78 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 4>>,
        /*  79 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 4>>,
        /*  80 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 4>>,
        /*  81 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 4>>,
        /*  82 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 4>>,
        /*  83 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 4>>,
        /*  84 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 4>>,
        /*  85 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 4>>,
        /*  86 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 4>>,
        /*  87 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 4>>,
        /*  88 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 4>>,
        /*  89 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 4>>,
        /*  90 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 4>>,
        /*  91 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 4>>,
        /*  92 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 4>>,
        /*  93 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 4>>,
        /*  94 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 4>>,
        /*  95 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 4>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 4>>,
        /*  96 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 8>>,
        /*  97 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 8>>,
        /*  98 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 8>>,
        /*  99 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 8>>,
        /* 100 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 8>>,
        /* 101 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 8>>,
        /* 102 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 8>>,
        /* 103 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 8>>,
        /* 104 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 8>>,
        /* 105 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 8>>,
        /* 106 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 8>>,
        /* 107 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 8>>,
        /* 108 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 8>>,
        /* 109 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 8>>,
        /* 110 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 8>>,
        /* 111 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 8>>,
        /* 112 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 8>>,
        /* 113 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 8>>,
        /* 114 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 8>>,
        /* 115 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 8>>,
        /* 116 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 8>>,
        /* 117 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 8>>,
        /* 118 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 8>>,
        /* 119 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 8>>,
        /* 120 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_N, 8>>,
        /* 121 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::X_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::X_P, 8>>,
        /* 122 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_N, 8>>,
        /* 123 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::Y_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::Y_P, 8>>,
        /* 124 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 8>>,
        /* 125 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::WHEEL_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::WHEEL_P, 8>>,
        /* 126 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 8>>,
        /* 127 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::MouseMove<MOUSEMOVE::PAN_N, 8>,
            /*  -  */ Tap::MouseMove<MOUSEMOVE::PAN_P, 8>>>,
    /* ch14 */ CC2::Selector<
        /* mod */ NoModify,
        /*   0 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_UP>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_DOWN>>,
        /*   1 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_LEFT>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_RIGHT>>,
        /*   2 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_VALUE_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_VALUE_DEC>>,
        /*   3 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_DEC>>,
        /*   4 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_DEC>>,
        /*   5 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::CHANNEL_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::CHANNEL_DEC>>,
        /*   6 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::SCAN_NEXT_TRACK>,
            /*  -  */ Tap::Consumer<CONSUMER::SCAN_PREVIOUS_TRACK>>,
        /*   7 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::FRAME_FORWARD>,
            /*  -  */ Tap::Consumer<CONSUMER::FRAME_BACK>>,
        /*   8 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::TRACKING_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::TRACKING_DEC>>,
        /*   9 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::SEARCH_MARK_FORWARD>,
            /*  -  */ Tap::Consumer<CONSUMER::SEARCH_MARK_BACKWARDS>>,
        /*  10 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::VOLUME_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::VOLUME_DEC>>,
        /*  11 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::Consumer<CONSUMER::BALANCE_RIGHT>,
            /*  -  */ Tap::Consumer<CONSUMER::BALANCE_LEFT>>,
        /*  12 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::Z, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::Z, MODKEY::CTRL_L>>,
        /*  13 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::Z, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::Z, MODKEY::GUI_L>>,
        /*  14 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT>>,
        /*  15 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L>>,
        /*  16 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  17 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  18 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  19 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L>>,
        /*  20 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  21 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  22 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::GUI_L>>,
        /*  23 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  24 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN>>,
        /*  25 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L>>,
        /*  26 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  27 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  28 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  29 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L>>,
        /*  30 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  31 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  32 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::GUI_L>>,
        /*  33 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  34 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS>>,
        /*  35 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L>>,
        /*  36 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  37 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  38 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  39 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L>>,
        /*  40 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  41 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  42 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::GUI_L>>,
        /*  43 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  44 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA>>,
        /*  45 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L>>,
        /*  46 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  47 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  48 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  49 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L>>,
        /*  50 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  51 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  52 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::GUI_L>>,
        /*  53 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  54 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT>>,
        /*  55 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L>>,
        /*  56 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  57 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  58 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  59 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L>>,
        /*  60 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  61 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  62 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::GUI_L>>,
        /*  63 */ CC2::RelativeEncoder<
            /* rel */ RelType::BinaryOffset,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  64 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_UP>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_DOWN>>,
        /*  65 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_LEFT>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_RIGHT>>,
        /*  66 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_VALUE_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_VALUE_DEC>>,
        /*  67 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_DEC>>,
        /*  68 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_DEC>>,
        /*  69 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::CHANNEL_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::CHANNEL_DEC>>,
        /*  70 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::SCAN_NEXT_TRACK>,
            /*  -  */ Tap::Consumer<CONSUMER::SCAN_PREVIOUS_TRACK>>,
        /*  71 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::FRAME_FORWARD>,
            /*  -  */ Tap::Consumer<CONSUMER::FRAME_BACK>>,
        /*  72 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::TRACKING_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::TRACKING_DEC>>,
        /*  73 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::SEARCH_MARK_FORWARD>,
            /*  -  */ Tap::Consumer<CONSUMER::SEARCH_MARK_BACKWARDS>>,
        /*  74 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::VOLUME_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::VOLUME_DEC>>,
        /*  75 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::Consumer<CONSUMER::BALANCE_RIGHT>,
            /*  -  */ Tap::Consumer<CONSUMER::BALANCE_LEFT>>,
        /*  76 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::Z, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::Z, MODKEY::CTRL_L>>,
        /*  77 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::Z, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::Z, MODKEY::GUI_L>>,
        /*  78 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT>>,
        /*  79 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L>>,
        /*  80 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  81 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  82 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  83 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L>>,
        /*  84 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  85 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  86 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::GUI_L>>,
        /*  87 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  88 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN>>,
        /*  89 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L>>,
        /*  90 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  91 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  92 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  93 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L>>,
        /*  94 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  95 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  96 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::GUI_L>>,
        /*  97 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  98 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS>>,
        /*  99 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L>>,
        /* 100 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /* 101 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /* 102 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /* 103 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L>>,
        /* 104 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /* 105 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /* 106 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::GUI_L>>,
        /* 107 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /* 108 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA>>,
        /* 109 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L>>,
        /* 110 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /* 111 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /* 112 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /* 113 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L>>,
        /* 114 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /* 115 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /* 116 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::GUI_L>>,
        /* 117 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /* 118 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT>>,
        /* 119 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L>>,
        /* 120 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /* 121 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /* 122 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /* 123 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L>>,
        /* 124 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /* 125 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /* 126 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::GUI_L>>,
        /* 127 */ CC2::RelativeEncoder<
            /* rel */ RelType::TwosComplement,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::GUI_L, MODKEY::SHIFT_L>>>,
    /* ch15 */ CC2::Selector<
        /* mod */ NoModify,
        /*   0 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_UP>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_DOWN>>,
        /*   1 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_LEFT>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_RIGHT>>,
        /*   2 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_VALUE_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_VALUE_DEC>>,
        /*   3 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_DEC>>,
        /*   4 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_DEC>>,
        /*   5 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::CHANNEL_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::CHANNEL_DEC>>,
        /*   6 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::SCAN_NEXT_TRACK>,
            /*  -  */ Tap::Consumer<CONSUMER::SCAN_PREVIOUS_TRACK>>,
        /*   7 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::FRAME_FORWARD>,
            /*  -  */ Tap::Consumer<CONSUMER::FRAME_BACK>>,
        /*   8 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::TRACKING_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::TRACKING_DEC>>,
        /*   9 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::SEARCH_MARK_FORWARD>,
            /*  -  */ Tap::Consumer<CONSUMER::SEARCH_MARK_BACKWARDS>>,
        /*  10 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::VOLUME_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::VOLUME_DEC>>,
        /*  11 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::Consumer<CONSUMER::BALANCE_RIGHT>,
            /*  -  */ Tap::Consumer<CONSUMER::BALANCE_LEFT>>,
        /*  12 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::Z, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::Z, MODKEY::CTRL_L>>,
        /*  13 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::Z, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::Z, MODKEY::GUI_L>>,
        /*  14 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT>>,
        /*  15 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L>>,
        /*  16 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  17 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  18 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  19 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L>>,
        /*  20 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  21 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  22 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::GUI_L>>,
        /*  23 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  24 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN>>,
        /*  25 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L>>,
        /*  26 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  27 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  28 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  29 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L>>,
        /*  30 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  31 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  32 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::GUI_L>>,
        /*  33 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  34 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS>>,
        /*  35 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L>>,
        /*  36 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  37 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  38 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  39 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L>>,
        /*  40 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  41 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  42 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::GUI_L>>,
        /*  43 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  44 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA>>,
        /*  45 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L>>,
        /*  46 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  47 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  48 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  49 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L>>,
        /*  50 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  51 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  52 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::GUI_L>>,
        /*  53 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  54 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT>>,
        /*  55 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L>>,
        /*  56 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  57 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  58 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  59 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L>>,
        /*  60 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  61 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  62 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::GUI_L>>,
        /*  63 */ CC2::RelativeEncoder<
            /* rel */ RelType::SignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  64 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_UP>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_DOWN>>,
        /*  65 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_LEFT>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_RIGHT>>,
        /*  66 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::MENU_VALUE_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::MENU_VALUE_DEC>>,
        /*  67 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_DEC>>,
        /*  68 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_DEC>>,
        /*  69 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::CHANNEL_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::CHANNEL_DEC>>,
        /*  70 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::SCAN_NEXT_TRACK>,
            /*  -  */ Tap::Consumer<CONSUMER::SCAN_PREVIOUS_TRACK>>,
        /*  71 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::FRAME_FORWARD>,
            /*  -  */ Tap::Consumer<CONSUMER::FRAME_BACK>>,
        /*  72 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::TRACKING_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::TRACKING_DEC>>,
        /*  73 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::SEARCH_MARK_FORWARD>,
            /*  -  */ Tap::Consumer<CONSUMER::SEARCH_MARK_BACKWARDS>>,
        /*  74 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::VOLUME_INC>,
            /*  -  */ Tap::Consumer<CONSUMER::VOLUME_DEC>>,
        /*  75 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::Consumer<CONSUMER::BALANCE_RIGHT>,
            /*  -  */ Tap::Consumer<CONSUMER::BALANCE_LEFT>>,
        /*  76 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::Z, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::Z, MODKEY::CTRL_L>>,
        /*  77 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::Z, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::Z, MODKEY::GUI_L>>,
        /*  78 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT>>,
        /*  79 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L>>,
        /*  80 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  81 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  82 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  83 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L>>,
        /*  84 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  85 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  86 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::GUI_L>>,
        /*  87 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_RIGHT, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_LEFT, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  88 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN>>,
        /*  89 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L>>,
        /*  90 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /*  91 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /*  92 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /*  93 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L>>,
        /*  94 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /*  95 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /*  96 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::GUI_L>>,
        /*  97 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::ARROW_UP, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::ARROW_DOWN, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /*  98 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS>>,
        /*  99 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L>>,
        /* 100 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /* 101 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /* 102 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /* 103 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L>>,
        /* 104 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /* 105 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /* 106 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::GUI_L>>,
        /* 107 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::EQUAL, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::MINUS, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /* 108 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA>>,
        /* 109 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L>>,
        /* 110 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /* 111 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /* 112 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /* 113 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L>>,
        /* 114 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /* 115 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /* 116 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::GUI_L>>,
        /* 117 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::PERIOD, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::COMMA, MODKEY::GUI_L, MODKEY::SHIFT_L>>,
        /* 118 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT>>,
        /* 119 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L>>,
        /* 120 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::SHIFT_L>>,
        /* 121 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::ALT_L>>,
        /* 122 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::CTRL_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::CTRL_L, MODKEY::GUI_L>>,
        /* 123 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L>>,
        /* 124 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L, MODKEY::SHIFT_L>>,
        /* 125 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::ALT_L, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::ALT_L, MODKEY::GUI_L>>,
        /* 126 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::GUI_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::GUI_L>>,
        /* 127 */ CC2::RelativeEncoder<
            /* rel */ RelType::InvSignBit,
            /*  +  */ Tap::KeyboardKey<KEY::BRACKET_RIGHT, MODKEY::GUI_L, MODKEY::SHIFT_L>,
            /*  -  */ Tap::KeyboardKey<KEY::BRACKET_LEFT, MODKEY::GUI_L, MODKEY::SHIFT_L>>>,
    /* ch16 */ CC2::Selector<
        /* mod */ NoModify,
        /*   0 */ CC2::Consumer<CONSUMER::POWER>,
        /*   1 */ CC2::Consumer<CONSUMER::RESET>,
        /*   2 */ CC2::Consumer<CONSUMER::SLEEP>,
        /*   3 */ CC2::Consumer<CONSUMER::SLEEP_AFTER>,
        /*   4 */ CC2::Consumer<CONSUMER::SLEEP_MODE>,
        /*   5 */ CC2::Consumer<CONSUMER::ILLUMINATION>,
        /*   6 */ CC2::Consumer<CONSUMER::VOICE_COMMAND>,
        /*   7 */ CC2::Consumer<CONSUMER::START_OR_STOP_VOICE_DICTATION_SESSION>,
        /*   8 */ CC2::Consumer<CONSUMER::INVOKE_DISMISS_EMOJI_PICKER>,
        /*   9 */ CC2::Consumer<CONSUMER::MENU>,
        /*  10 */ CC2::Consumer<CONSUMER::MENU_PICK>,
        /*  11 */ CC2::Consumer<CONSUMER::MENU_UP>,
        /*  12 */ CC2::Consumer<CONSUMER::MENU_DOWN>,
        /*  13 */ CC2::Consumer<CONSUMER::MENU_LEFT>,
        /*  14 */ CC2::Consumer<CONSUMER::MENU_RIGHT>,
        /*  15 */ CC2::Consumer<CONSUMER::MENU_ESCAPE>,
        /*  16 */ CC2::Consumer<CONSUMER::MENU_VALUE_INC>,
        /*  17 */ CC2::Consumer<CONSUMER::MENU_VALUE_DEC>,
        /*  18 */ CC2::Consumer<CONSUMER::DATA_ON_SCREEN>,
        /*  19 */ CC2::Consumer<CONSUMER::CLOSED_CAPTION>,
        /*  20 */ CC2::Consumer<CONSUMER::CLOSED_CAPTION_SELECT>,
        /*  21 */ CC2::Consumer<CONSUMER::VCR_TV>,
        /*  22 */ CC2::Consumer<CONSUMER::BROADCAST_MODE>,
        /*  23 */ CC2::Consumer<CONSUMER::SNAPSHOT>,
        /*  24 */ CC2::Consumer<CONSUMER::STILL>,
        /*  25 */ CC2::Consumer<CONSUMER::PICTURE_IN_PICTURE_TOGGLE>,
        /*  26 */ CC2::Consumer<CONSUMER::PICTURE_IN_PICTURE_SWAP>,
        /*  27 */ CC2::Consumer<CONSUMER::ASPECT>,
        /*  28 */ CC2::Consumer<CONSUMER::D3D_MODE_SELECT>,
        /*  29 */ CC2::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_INC>,
        /*  30 */ CC2::Consumer<CONSUMER::DISPLAY_BRIGHTNESS_DEC>,
        /*  31 */ CC2::Consumer<CONSUMER::DISPLAY_BACKLIGHT_TOGGLE>,
        /*  32 */ CC2::Consumer<CONSUMER::DISPLAY_SET_BRIGHTNESS_TO_MIN>,
        /*  33 */ CC2::Consumer<CONSUMER::DISPLAY_SET_BRIGHTNESS_TO_MAX>,
        /*  34 */ CC2::Consumer<CONSUMER::DISPLAY_SET_AUTO_BRIGHTNESS>,
        /*  35 */ CC2::Consumer<CONSUMER::CAMERA_ACCESS_ENABLED>,
        /*  36 */ CC2::Consumer<CONSUMER::CAMERA_ACCESS_DISABLED>,
        /*  37 */ CC2::Consumer<CONSUMER::CAMERA_ACCESS_TOGGLE>,
        /*  38 */ CC2::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_INC>,
        /*  39 */ CC2::Consumer<CONSUMER::KEYBOARD_BRIGHTNESS_DEC>,
        /*  40 */ CC2::Consumer<CONSUMER::KEYBOARD_BACKLIGHT_OOC>,
        /*  41 */ CC2::Consumer<CONSUMER::KEYBOARD_BACKLIGHT_SET_MIN>,
        /*  42 */ CC2::Consumer<CONSUMER::KEYBOARD_BACKLIGHT_SET_MAX>,
        /*  43 */ CC2::Consumer<CONSUMER::KEYBOARD_BACKLIGHT_AUTO>,
        /*  44 */ CC2::Consumer<CONSUMER::ASSIGN_SELECTION>,
        /*  45 */ CC2::Consumer<CONSUMER::MODE_STEP>,
        /*  46 */ CC2::Consumer<CONSUMER::RECALL_LAST>,
        /*  47 */ CC2::Consumer<CONSUMER::ENTER_CHANNEL>,
        /*  48 */ CC2::Consumer<CONSUMER::ORDER_MOVIE>,
        /*  49 */ CC2::Consumer<CONSUMER::QUIT>,
        /*  50 */ CC2::Consumer<CONSUMER::HELP>,
        /*  51 */ CC2::Consumer<CONSUMER::CHANNEL_INC>,
        /*  52 */ CC2::Consumer<CONSUMER::CHANNEL_DEC>,
        /*  53 */ CC2::Consumer<CONSUMER::VCR_PLUS>,
        /*  54 */ CC2::Consumer<CONSUMER::ONCE>,
        /*  55 */ CC2::Consumer<CONSUMER::DAILY>,
        /*  56 */ CC2::Consumer<CONSUMER::WEEKLY>,
        /*  57 */ CC2::Consumer<CONSUMER::MONTHLY>,
        /*  58 */ CC2::Consumer<CONSUMER::PLAY>,
        /*  59 */ CC2::Consumer<CONSUMER::PAUSE>,
        /*  60 */ CC2::Consumer<CONSUMER::RECORD>,
        /*  61 */ CC2::Consumer<CONSUMER::FAST_FORWARD>,
        /*  62 */ CC2::Consumer<CONSUMER::REWIND>,
        /*  63 */ CC2::Consumer<CONSUMER::SCAN_NEXT_TRACK>,
        /*  64 */ CC2::Consumer<CONSUMER::SCAN_PREVIOUS_TRACK>,
        /*  65 */ CC2::Consumer<CONSUMER::STOP>,
        /*  66 */ CC2::Consumer<CONSUMER::EJECT>,
        /*  67 */ CC2::Consumer<CONSUMER::RANDOM_PLAY>,
        /*  68 */ CC2::Consumer<CONSUMER::ENTER_DISC>,
        /*  69 */ CC2::Consumer<CONSUMER::REPEAT>,
        /*  70 */ CC2::Consumer<CONSUMER::TRACK_NORMAL>,
        /*  71 */ CC2::Consumer<CONSUMER::FRAME_FORWARD>,
        /*  72 */ CC2::Consumer<CONSUMER::FRAME_BACK>,
        /*  73 */ CC2::Consumer<CONSUMER::TRACKING_INC>,
        /*  74 */ CC2::Consumer<CONSUMER::TRACKING_DEC>,
        /*  75 */ CC2::Consumer<CONSUMER::STOP_EJECT>,
        /*  76 */ CC2::Consumer<CONSUMER::PLAY_PAUSE>,
        /*  77 */ CC2::Consumer<CONSUMER::PLAY_SKIP>,
        /*  78 */ CC2::Consumer<CONSUMER::MARK>,
        /*  79 */ CC2::Consumer<CONSUMER::CLEAR_MARK>,
        /*  80 */ CC2::Consumer<CONSUMER::REPEAT_FROM_MARK>,
        /*  81 */ CC2::Consumer<CONSUMER::RETURN_TO_MARK>,
        /*  82 */ CC2::Consumer<CONSUMER::SEARCH_MARK_FORWARD>,
        /*  83 */ CC2::Consumer<CONSUMER::SEARCH_MARK_BACKWARDS>,
        /*  84 */ CC2::Consumer<CONSUMER::COUNTER_RESET>,
        /*  85 */ CC2::Consumer<CONSUMER::SHOW_COUNTER>,
        /*  86 */ CC2::Consumer<CONSUMER::MUTE>,
        /*  87 */ CC2::Consumer<CONSUMER::BASS_BOOST>,
        /*  88 */ CC2::Consumer<CONSUMER::SURROUND_MODE>,
        /*  89 */ CC2::Consumer<CONSUMER::LOUDNESS>,
        /*  90 */ CC2::Consumer<CONSUMER::MPX>,
        /*  91 */ CC2::Consumer<CONSUMER::VOLUME_INC>,
        /*  92 */ CC2::Consumer<CONSUMER::VOLUME_DEC>,
        /*  93 */ CC2::Consumer<CONSUMER::SPEED_SELECT>,
        /*  94 */ CC2::Consumer<CONSUMER::SLOW>,
        /*  95 */ CC2::Consumer<CONSUMER::FAN_ENABLE>,
        /*  96 */ CC2::Consumer<CONSUMER::LIGHT_ENABLE>,
        /*  97 */ CC2::Consumer<CONSUMER::CLIMATE_CONTROL_ENABLE>,
        /*  98 */ CC2::Consumer<CONSUMER::SECURITY_ENABLE>,
        /*  99 */ CC2::Consumer<CONSUMER::FIRE_ALARM>,
        /* 100 */ CC2::Consumer<CONSUMER::POLICE_ALARM>,
        /* 101 */ CC2::Consumer<CONSUMER::MOTION>,
        /* 102 */ CC2::Consumer<CONSUMER::DURESS_ALARM>,
        /* 103 */ CC2::Consumer<CONSUMER::HOLDUP_ALARM>,
        /* 104 */ CC2::Consumer<CONSUMER::MEDICAL_ALARM>,
        /* 105 */ CC2::Consumer<CONSUMER::BALANCE_RIGHT>,
        /* 106 */ CC2::Consumer<CONSUMER::BALANCE_LEFT>,
        /* 107 */ CC2::Consumer<CONSUMER::BASS_INC>,
        /* 108 */ CC2::Consumer<CONSUMER::BASS_DEC>,
        /* 109 */ CC2::Consumer<CONSUMER::TREBLE_INC>,
        /* 110 */ CC2::Consumer<CONSUMER::TREBLE_DEC>,
        /* 111 */ CC2::Consumer<CONSUMER::SUB_CHANNEL_INC>,
        /* 112 */ CC2::Consumer<CONSUMER::SUB_CHANNEL_DEC>,
        /* 113 */ CC2::Consumer<CONSUMER::ALTERNATE_AUDIO_INC>,
        /* 114 */ CC2::Consumer<CONSUMER::ALTERNATE_AUDIO_DEC>>>
    kCCTable{};
// clang-format on

// clang-format off
inline static constexpr Prog2::FunctionTable<
    /* ch 1 */ Prog2::Keyboard<0x00>,
    /* ch 2 */ Prog2::Keyboard<0x80>,
    /* ch 3 */ Prog2::Keyboard<0x00, MODKEY::CTRL_L>,
    /* ch 4 */ Prog2::Keyboard<0x00, MODKEY::CTRL_L, MODKEY::SHIFT_L>,
    /* ch 5 */ Prog2::Keyboard<0x00, MODKEY::CTRL_L, MODKEY::ALT_L>,
    /* ch 6 */ Prog2::Keyboard<0x00, MODKEY::CTRL_L, MODKEY::GUI_L>,
    /* ch 7 */ Prog2::Keyboard<0x00, MODKEY::ALT_L>,
    /* ch 8 */ Prog2::Keyboard<0x00, MODKEY::ALT_L, MODKEY::SHIFT_L>,
    /* ch 9 */ Prog2::Keyboard<0x00, MODKEY::ALT_L, MODKEY::GUI_L>,
    /* ch10 */ Prog2::Keyboard<0x00, MODKEY::GUI_L>,
    /* ch11 */ Prog2::Keyboard<0x00, MODKEY::GUI_L, MODKEY::SHIFT_L>,
    /* ch12 */ Prog2::Keyboard<0x00, MODKEY::SHIFT_L>,
    /* ch13 */ Prog2::Selector<
        /* mod */ MouseDataModify,
        /*   0 */ Prog2::MouseMove<MOUSEMOVE::X_P>,
        /*   1 */ Prog2::MouseMove<MOUSEMOVE::X_N>,
        /*   2 */ Prog2::MouseMove<MOUSEMOVE::Y_P>,
        /*   3 */ Prog2::MouseMove<MOUSEMOVE::Y_N>,
        /*   4 */ Prog2::MouseMove<MOUSEMOVE::WHEEL_P>,
        /*   5 */ Prog2::MouseMove<MOUSEMOVE::WHEEL_N>,
        /*   6 */ Prog2::MouseMove<MOUSEMOVE::PAN_P>,
        /*   7 */ Prog2::MouseMove<MOUSEMOVE::PAN_N>,
        /*   8 */ Blank,
        /*   9 */ Prog2::MouseButton<MOUSEBUTTON::LEFT>,
        /*  10 */ Prog2::MouseButton<MOUSEBUTTON::MIDDLE>,
        /*  11 */ Prog2::MouseButton<MOUSEBUTTON::RIGHT>>,
    /* ch14 */ Prog2::AppLancher,
    /* ch15 */ Prog2::StringList<kCmdList.size(), kCmdList>,
    /* ch16 */ Prog2::StringList<kTwitchEmoteList.size(), kTwitchEmoteList>>
    kProgTable{};
// clang-format on

uint8_t MouseDataModify(uint8_t data1, uint8_t data2, size_t tablesize) {
  const auto upper_octave{0x7F / tablesize};
  const auto octave{data1 / tablesize};
  return octave == upper_octave ? data2 : octave + 1;
}

uint8_t GamePadDataModify(uint8_t data1, uint8_t data2, size_t tablesize) {
  const auto upper_octave{0x7F / tablesize};
  const auto octave{data1 / tablesize};
  return octave == upper_octave       ? data2
         : octave == upper_octave - 1 ? 0x7F
                                      : (octave + 1) * (0x7F / upper_octave);
}

}  // namespace commontable

}  // namespace midi2usb
#pragma once

#include <algorithm>
#include <iterator>

#include "../../debug.hpp"
#include "../../usb/report.hpp"

namespace midi2usb {  // declaration

template <class T>
bool SetBit(T* tgt, T flag) {
  if ((*tgt & flag) != 0) {
    return false;  // not update

  } else {
    *tgt |= flag;
    return true;  // update
  }
}

template <class T>
bool ResetBit(T* tgt, T flag) {
  if ((*tgt & flag) == 0) {
    return false;  // not update

  } else {
    *tgt &= ~flag;
    return true;  // update
  }
}

template <class ReportBufferType>
class ReportMakerBase {
  using ValueType = typename ReportBufferType::ValueType;

 public:
  virtual void Write(ReportBufferType* buf);
  void SetTap() { tap_ = true; }

 protected:
  void ClearBuffer() { report_ = ValueType{}; }

  ValueType report_;
  bool update_;
  bool tap_;
};

class KeyRepoMaker
    : public ReportMakerBase<ReportBuffers::KeyboarReportBuffer> {
  using Buffer = ReportBuffers::KeyboarReportBuffer;
  using Base = ReportMakerBase<Buffer>;

 public:
  void Write(Buffer* buf) override;
  void PushMod(uint8_t mod) { update_ |= SetBit(&report_.modifier, mod); }
  void PopMod(uint8_t mod) { update_ |= ResetBit(&report_.modifier, mod); }
  void PushKey(uint8_t keycode);
  void PopKey(uint8_t keycode);
  void PushStr(const char* str) { str_ = str; }
  void Clear();

 private:
  void ReadStr();
  void Erase(uint8_t* ptr);

  static constexpr uint8_t kModKeyBeg{0xE0};
  uint8_t* tail_{report_.keycode};
  const char* str_{nullptr};
  char prev_char_{'\0'};
  int unsigned str_weight_{0};
};

class MouseRepoMaker
    : public ReportMakerBase<ReportBuffers::MouseReportBuffer> {
  using Buffer = ReportBuffers::MouseReportBuffer;
  using Base = ReportMakerBase<Buffer>;

 public:
  void Write(Buffer* buf) override;
  void PushBtn(uint8_t button) { update_ |= SetBit(&report_.button, button); }
  void PopBtn(uint8_t button) { update_ |= ResetBit(&report_.button, button); }
  void PushMove(unsigned idx, int8_t n);
  void PopMove(unsigned idx);
  void Clear();

 private:
  int8_t refcnt_[sizeof(report_.move)]{0};
};

class GameRepoMaker
    : public ReportMakerBase<ReportBuffers::GamePadReportBuffer> {
 public:
  void PushBtn(uint16_t button) { update_ |= SetBit(&report_.button, button); }
  void PopBtn(uint16_t button) { update_ |= ResetBit(&report_.button, button); }
  void PushAxis(unsigned idx, int8_t n);
  void PopAxis(unsigned idx);
  void Clear();

 private:
  int8_t refcnt_[sizeof(report_.axis)]{0};
};

class ConsumerRepoMaker
    : public ReportMakerBase<ReportBuffers::ConsumerReportBuffer> {
 public:
  void Push(uint16_t id);
  void Pop();
  void Clear();
};

struct ReportMakers {
  KeyRepoMaker keyboard;
  MouseRepoMaker mouse;
  GameRepoMaker gamepad;
  ConsumerRepoMaker consumer;
};

}  // namespace midi2usb

namespace midi2usb {  // declaration

template <class ReportBufferType>
void ReportMakerBase<ReportBufferType>::Write(ReportBufferType* buf) {
  if (update_) {
    buf->Push(report_);
    update_ = false;

    if (tap_) {
      ClearBuffer();
      buf->Push(report_);
      tap_ = false;
    }
  }
}

void KeyRepoMaker::Write(Buffer* buf) {
  if (str_ && str_weight_ == 0) ReadStr();
  str_weight_ = (str_weight_ + 1) & 0b11;
  Base::Write(buf);
}

void KeyRepoMaker::PushKey(uint8_t keycode) {
  if (keycode == 0) {
    // do nothing
    return;

  } else if (keycode >= kModKeyBeg) {
    PushMod(1u << (keycode - kModKeyBeg));

  } else if (report_.keycode == tail_) {
    report_.keycode[0] = keycode;
    ++tail_;
    update_ = true;

  } else if (std::find(report_.keycode, tail_, keycode) == tail_) {
    if (std::end(report_.keycode) == tail_) Erase(report_.keycode);
    *tail_ = keycode;
    ++tail_;
    update_ = true;
  }
}

void KeyRepoMaker::PopKey(uint8_t keycode) {
  if (keycode == 0) {
    // do nothing
    return;

  } else if (keycode >= kModKeyBeg) {
    PopMod(1u << (keycode - kModKeyBeg));

  } else if (report_.keycode != tail_) {
    const auto ptr{std::find(report_.keycode, tail_, keycode)};
    if (ptr != tail_) Erase(ptr);
  }
}

void KeyRepoMaker::ReadStr() {
  constexpr uint8_t kAscii2KeyTable[128][2]{HID_ASCII_TO_KEYCODE};

  report_ = KeyboarReport{};
  tail_ = report_.keycode;
  if (*str_ < 1) {
    str_ = nullptr;
    prev_char_ = '\0';
    str_weight_ = 0;
    update_ = true;

  } else if (*str_ == prev_char_) {
    prev_char_ = '\0';
    update_ = true;

  } else {
    if (kAscii2KeyTable[*str_][0] == 1) PushMod(2);
    PushKey(kAscii2KeyTable[*str_][1]);
    prev_char_ = *str_;
    ++str_;
  }
}

void KeyRepoMaker::Erase(uint8_t* ptr) {
  std::copy(ptr + 1, tail_, ptr);
  --tail_;
  *tail_ = 0;
  update_ = true;
}

void KeyRepoMaker::Clear() {
  ClearBuffer();
  tail_ = report_.keycode;
  str_ = nullptr;
  prev_char_ = '\0';
  str_weight_ = 0;
}

void MouseRepoMaker::Write(Buffer* buf) {
  update_ |= std::any_of(std::begin(refcnt_), std::end(refcnt_),
                         [](auto ref) { return ref != 0; });
  if (tap_) std::fill(std::begin(refcnt_), std::end(refcnt_), 0);
  Base::Write(buf);
}

void MouseRepoMaker::PushMove(unsigned idx, int8_t n) {
  ++refcnt_[idx];
  report_.move[idx] = n;
  update_ = true;
}

void MouseRepoMaker::PopMove(unsigned idx) {
  --refcnt_[idx];
  if (refcnt_[idx] <= 0) {
    refcnt_[idx] = 0;
    report_.move[idx] = 0;
    update_ = true;
  }
}

void MouseRepoMaker::Clear() {
  ClearBuffer();
  std::fill(std::begin(refcnt_), std::end(refcnt_), 0);
}

void GameRepoMaker::PushAxis(unsigned idx, int8_t n) {
  ++refcnt_[idx];
  report_.axis[idx] = static_cast<uint8_t>(n + report_.kAxisNeutral);
  update_ = true;
}

void GameRepoMaker::PopAxis(unsigned idx) {
  --refcnt_[idx];
  if (refcnt_[idx] <= 0) {
    refcnt_[idx] = 0;
    report_.axis[idx] = report_.kAxisNeutral;
    update_ = true;
  }
}

void GameRepoMaker::Clear() {
  ClearBuffer();
  std::fill(std::begin(refcnt_), std::end(refcnt_), 0);
}

void ConsumerRepoMaker::Push(uint16_t id) {
  report_.id = id;
  update_ = true;
}

void ConsumerRepoMaker::Pop() {
  report_.id = 0;
  update_ = true;
}

void ConsumerRepoMaker::Clear() { ClearBuffer(); }

}  // namespace midi2usb
#pragma once

#include <type_traits>

namespace midi2usb {

template <class Enum>
inline constexpr auto Enum2Num(Enum e) {
  return static_cast<typename std::underlying_type<Enum>::type>(e);
}

template <class Enum, class... Enums>
inline constexpr Enum EnumAND(Enum e, Enums... enums) {
  if constexpr (sizeof...(Enums) == 0) {
    return e;

  } else {
    return static_cast<Enum>(Enum2Num(e) & (Enum2Num(enums) & ...));
  }
}

template <class Enum, class... Enums>
inline constexpr Enum EnumOR(Enum e, Enums... enums) {
  if constexpr (sizeof...(Enums) == 0) {
    return e;

  } else {
    return static_cast<Enum>(Enum2Num(e) | (Enum2Num(enums) | ...));
  }
}

template <class Enum, class... Enums>
inline constexpr bool EnumTest(Enum e, Enums... enums) {
  return Enum2Num(EnumAND<Enum>(e, enums...)) != 0;
}

template <class Enum>
inline constexpr unsigned EnumBIT(const Enum e, const unsigned cur = 0) {
  return (Enum2Num(e) & (1 << cur)) != 0 ? cur : EnumBIT(e, cur + 1);
}

// clang-format off

enum class KEY : uint8_t {
  ERROR_ROLL_OVER        = 0x01,
  POST_FAIL              = 0x02,
  ERROR_UNDEFINED        = 0x03,
  A                      = 0x04,
  B                      = 0x05,
  C                      = 0x06,
  D                      = 0x07,
  E                      = 0x08,
  F                      = 0x09,
  G                      = 0x0A,
  H                      = 0x0B,
  I                      = 0x0C,
  J                      = 0x0D,
  K                      = 0x0E,
  L                      = 0x0F,
  M                      = 0x10,
  N                      = 0x11,
  O                      = 0x12,
  P                      = 0x13,
  Q                      = 0x14,
  R                      = 0x15,
  S                      = 0x16,
  T                      = 0x17,
  U                      = 0x18,
  V                      = 0x19,
  W                      = 0x1A,
  X                      = 0x1B,
  Y                      = 0x1C,
  Z                      = 0x1D,
  NO1                    = 0x1E,
  NO2                    = 0x1F,
  NO3                    = 0x20,
  NO4                    = 0x21,
  NO5                    = 0x22,
  NO6                    = 0x23,
  NO7                    = 0x24,
  NO8                    = 0x25,
  NO9                    = 0x26,
  NO0                    = 0x27,
  RETURN                 = 0x28,
  ESCAPE                 = 0x29,
  BACKSPACE              = 0x2A,
  TAB                    = 0x2B,
  SPACE                  = 0x2C,
  MINUS                  = 0x2D,
  EQUAL                  = 0x2E,
  BRACKET_LEFT           = 0x2F,
  BRACKET_RIGHT          = 0x30,
  BACKSLASH              = 0x31,
  EUROPE_1               = 0x32,
  SEMICOLON              = 0x33,
  APOSTROPHE             = 0x34,
  GRAVE                  = 0x35,
  COMMA                  = 0x36,
  PERIOD                 = 0x37,
  SLASH                  = 0x38,
  CAPS_LOCK              = 0x39,
  F1                     = 0x3A,
  F2                     = 0x3B,
  F3                     = 0x3C,
  F4                     = 0x3D,
  F5                     = 0x3E,
  F6                     = 0x3F,
  F7                     = 0x40,
  F8                     = 0x41,
  F9                     = 0x42,
  F10                    = 0x43,
  F11                    = 0x44,
  F12                    = 0x45,
  PRINT_SCREEN           = 0x46,
  SCROLL_LOCK            = 0x47,
  PAUSE                  = 0x48,
  INSERT                 = 0x49,
  HOME                   = 0x4A,
  PAGE_UP                = 0x4B,
  DELETE                 = 0x4C,
  END                    = 0x4D,
  PAGE_DOWN              = 0x4E,
  ARROW_RIGHT            = 0x4F,
  ARROW_LEFT             = 0x50,
  ARROW_DOWN             = 0x51,
  ARROW_UP               = 0x52,
  NUM_LOCK               = 0x53,
  KEYPAD_DIVIDE          = 0x54,
  KEYPAD_MULTIPLY        = 0x55,
  KEYPAD_SUBTRACT        = 0x56,
  KEYPAD_ADD             = 0x57,
  KEYPAD_ENTER           = 0x58,
  KEYPAD_1               = 0x59,
  KEYPAD_2               = 0x5A,
  KEYPAD_3               = 0x5B,
  KEYPAD_4               = 0x5C,
  KEYPAD_5               = 0x5D,
  KEYPAD_6               = 0x5E,
  KEYPAD_7               = 0x5F,
  KEYPAD_8               = 0x60,
  KEYPAD_9               = 0x61,
  KEYPAD_0               = 0x62,
  KEYPAD_PERIOD          = 0x63,
  EUROPE_2               = 0x64,
  APPLICATION            = 0x65,
  POWER                  = 0x66,
  KEYPAD_EQUAL           = 0x67,
  F13                    = 0x68,
  F14                    = 0x69,
  F15                    = 0x6A,
  F16                    = 0x6B,
  F17                    = 0x6C,
  F18                    = 0x6D,
  F19                    = 0x6E,
  F20                    = 0x6F,
  F21                    = 0x70,
  F22                    = 0x71,
  F23                    = 0x72,
  F24                    = 0x73,
  EXECUTE                = 0x74,
  HELP                   = 0x75,
  MENU                   = 0x76,
  SELECT                 = 0x77,
  STOP                   = 0x78,
  AGAIN                  = 0x79,
  UNDO                   = 0x7A,
  CUT                    = 0x7B,
  COPY                   = 0x7C,
  PASTE                  = 0x7D,
  FIND                   = 0x7E,
  MUTE                   = 0x7F,
  VOLUME_UP              = 0x80,
  VOLUME_DOWN            = 0x81,
  LOCKING_CAPS_LOCK      = 0x82,
  LOCKING_NUM_LOCK       = 0x83,
  LOCKING_SCROLL_LOCK    = 0x84,
  KEYPAD_COMMA           = 0x85,
  KEYPAD_EQUAL_SIGN      = 0x86,
  INTERNATIONAL1         = 0x87,
  INTERNATIONAL2         = 0x88,
  INTERNATIONAL3         = 0x89,
  INTERNATIONAL4         = 0x8A,
  INTERNATIONAL5         = 0x8B,
  INTERNATIONAL6         = 0x8C,
  INTERNATIONAL7         = 0x8D,
  INTERNATIONAL8         = 0x8E,
  INTERNATIONAL9         = 0x8F,
  LANG1                  = 0x90,
  LANG2                  = 0x91,
  LANG3                  = 0x92,
  LANG4                  = 0x93,
  LANG5                  = 0x94,
  LANG6                  = 0x95,
  LANG7                  = 0x96,
  LANG8                  = 0x97,
  LANG9                  = 0x98,
  ALTERNATE_ERASE        = 0x99,
  SYSREQ_ATTENTION       = 0x9A,
  CANCEL                 = 0x9B,
  CLEAR                  = 0x9C,
  PRIOR                  = 0x9D,
  RETURN2                = 0x9E,
  SEPARATOR              = 0x9F,
  OUT                    = 0xA0,
  OPER                   = 0xA1,
  CLEAR_AGAIN            = 0xA2,
  CRSEL_PROPS            = 0xA3,
  EXSEL                  = 0xA4,
  KEYPAD_00              = 0xB0,
  KEYPAD_000             = 0xB1,
  THONSANDS_SEPARATOR    = 0xB2,
  DECIMAL_SEPARATOR      = 0xB3,
  CURRENCY_UNIT32        = 0xB4,
  CURRENCY_SUBUNIT       = 0xB5,
  KEYPAD_PAREN_LEFT      = 0xB6,
  KEYPAD_PAREN_RIGHT     = 0xB7,
  KEYPAD_BRACE_LEFT      = 0xB8,
  KEYPAD_BRACE_RIGHT     = 0xB9,
  KEYPAD_TAB             = 0xBA,
  KEYPAD_BACKSPACE       = 0xBB,
  KEYPAD_A               = 0xBC,
  KEYPAD_B               = 0xBD,
  KEYPAD_C               = 0xBE,
  KEYPAD_D               = 0xBF,
  KEYPAD_E               = 0xC0,
  KEYPAD_F               = 0xC1,
  KEYPAD_XOR             = 0xC2,
  KEYPAD_CARET           = 0xC3,
  KEYPAD_PERCENT         = 0xC4,
  KEYPAD_ANGLE_LEFT      = 0xC5,
  KEYPAD_ANGLE_RIGHT     = 0xC6,
  KEYPAD_AND             = 0xC7,
  KEYPAD_AND_DOUBLE      = 0xC8,
  KEYPAD_VERTICAL        = 0xC9,
  KEYPAD_VERTICAL_DOUBLE = 0xCA,
  KEYPAD_COLON           = 0xCB,
  KEYPAD_NUM_SIGN        = 0xCC,
  KEYPAD_SPACE           = 0xCD,
  KEYPAD_AT_SIGN         = 0xCE,
  KEYPAD_EXCLAMATION     = 0xCF,
  KEYPAD_MEM_STORE       = 0xD0,
  KEYPAD_MEM_RECALL      = 0xD1,
  KEYPAD_MEM_CLEAR       = 0xD2,
  KEYPAD_MEM_ADD         = 0xD3,
  KEYPAD_MEM_SUBTRACT    = 0xD4,
  KEYPAD_MEM_MULTIPLY    = 0xD5,
  KEYPAD_MEM_DIVIDE      = 0xD6,
  KEYPAD_PLUS_MINUS      = 0xD7,
  KEYPAD_CLEAR           = 0xD8,
  KEYPAD_CLEAR_ENTRY     = 0xD9,
  KEYPAD_BINARY          = 0xDA,
  KEYPAD_OCTAL           = 0xDB,
  KEYPAD_DECIMAL         = 0xDC,
  KEYPAD_HEXADECIMAL     = 0xDD,
  CTRL_L                 = 0xE0,
  SHIFT_L                = 0xE1,
  ALT_L                  = 0xE2,
  GUI_L                  = 0xE3,
  CTRL_R                 = 0xE4,
  SHIFT_R                = 0xE5,
  ALT_R                  = 0xE6,
  GUI_R                  = 0xE7
};

enum class MODKEY : uint8_t {
  NONE    = 0,
  CTRL_L  = 1 << 0,
  SHIFT_L = 1 << 1,
  ALT_L   = 1 << 2,
  GUI_L   = 1 << 3,
  CTRL_R  = 1 << 4,
  SHIFT_R = 1 << 5,
  ALT_R   = 1 << 6,
  GUI_R   = 1 << 7,
};

enum class MOUSEBUTTON : uint8_t {
  NONE     = 0,
  LEFT     = 1 << 0,
  RIGHT    = 1 << 1,
  MIDDLE   = 1 << 2,
  BACKWARD = 1 << 3,
  FORWARD  = 1 << 4
};

enum class MOUSEMOVE : uint8_t {
  NONE      = 0,
  X_P       = 1 << 0,
  Y_P       = 1 << 1,
  WHEEL_P   = 1 << 2,
  PAN_P     = 1 << 3,
  _NEGATIVE = 128,
  X_N       = X_P     | _NEGATIVE,
  Y_N       = Y_P     | _NEGATIVE,
  WHEEL_N   = WHEEL_P | _NEGATIVE,
  PAN_N     = PAN_P   | _NEGATIVE,
};

enum class GAMEPADBUTTON : uint16_t {
  NONE = 0,
  NO1  = 1 << 0,
  NO2  = 1 << 1,
  NO3  = 1 << 2,
  NO4  = 1 << 3,
  NO5  = 1 << 4,
  NO6  = 1 << 5,
  NO7  = 1 << 6,
  NO8  = 1 << 7,
  NO9  = 1 << 8,
  NO10 = 1 << 9,
  NO11 = 1 << 10,
  NO12 = 1 << 11,
  NO13 = 1 << 12,
  NO14 = 1 << 13,
  NO15 = 1 << 14,
  NO16 = 1 << 15
};

enum class GAMEPADAXIS : uint8_t {
  NONE      = 0,
  X_P       = 1 << 0,
  Y_P       = 1 << 1,
  Z_P       = 1 << 2,
  RZ_P      = 1 << 3,
  _NEGATIVE = 128,
  X_N       = X_P  | _NEGATIVE,
  Y_N       = Y_P  | _NEGATIVE,
  Z_N       = Z_P  | _NEGATIVE,
  RZ_N      = RZ_P | _NEGATIVE,
};

enum class CONSUMER : uint16_t {
  _MASK                                 = 0x0FFF,
  _OOC                                  = (1 << 0) << 12,
  _MC                                   = (1 << 1) << 12,
  _OSC                                  = (1 << 2) << 12,
  _RTC                                  = (1 << 3) << 12,

  NONE                                  = 0x0000,

  // Numeric Key Pad
  PLUS10                                = 0x0020 | _OSC,
  PLUS100                               = 0x0021 | _OSC,
  AM_PM                                 = 0x0022 | _OSC,

  // General Controls
  POWER                                 = 0x0030 | _OOC,
  RESET                                 = 0x0031 | _OSC,
  SLEEP                                 = 0x0032 | _OSC,
  SLEEP_AFTER                           = 0x0033 | _OSC,
  SLEEP_MODE                            = 0x0034 | _RTC,
  ILLUMINATION                          = 0x0035 | _OOC,
  VOICE_COMMAND                         = 0x00CF | _OSC,
  START_OR_STOP_VOICE_DICTATION_SESSION = 0x00D8 | _OOC,
  INVOKE_DISMISS_EMOJI_PICKER           = 0x00D9 | _OOC,

  // Menu Controls
  MENU                                  = 0x0040 | _OOC,
  MENU_PICK                             = 0x0041 | _OSC,
  MENU_UP                               = 0x0042 | _OSC,
  MENU_DOWN                             = 0x0043 | _OSC,
  MENU_LEFT                             = 0x0044 | _OSC,
  MENU_RIGHT                            = 0x0045 | _OSC,
  MENU_ESCAPE                           = 0x0046 | _OSC,
  MENU_VALUE_INC                        = 0x0047 | _OSC,
  MENU_VALUE_DEC                        = 0x0048 | _OSC,
  RED_MENU_BUTTON                       = 0x0069 | _MC,
  GREEN_MENU_BUTTON                     = 0x006A | _MC,
  BLUE_MENU_BUTTON                      = 0x006B | _MC,
  YELLOW_MENU_BUTTON                    = 0x006C | _MC,

  // Display Controls
  DATA_ON_SCREEN                        = 0x0060 | _OOC,
  CLOSED_CAPTION                        = 0x0061 | _OOC,
  CLOSED_CAPTION_SELECT                 = 0x0062 | _OSC,
  VCR_TV                                = 0x0063 | _OOC,
  BROADCAST_MODE                        = 0x0064 | _OSC,
  SNAPSHOT                              = 0x0065 | _OSC,
  STILL                                 = 0x0066 | _OSC,
  PICTURE_IN_PICTURE_TOGGLE             = 0x0067 | _OSC,
  PICTURE_IN_PICTURE_SWAP               = 0x0068 | _OSC,
  ASPECT                                = 0x006D | _OSC,
  D3D_MODE_SELECT                       = 0x006E | _OSC,
  DISPLAY_BRIGHTNESS_INC                = 0x006F | _RTC,
  DISPLAY_BRIGHTNESS_DEC                = 0x0070 | _RTC,
  // DISPLAY_BRIGHTNESS                    = 0x0071 | _LC,
  DISPLAY_BACKLIGHT_TOGGLE              = 0x0072 | _OOC,
  DISPLAY_SET_BRIGHTNESS_TO_MIN         = 0x0073 | _OSC,
  DISPLAY_SET_BRIGHTNESS_TO_MAX         = 0x0074 | _OSC,
  DISPLAY_SET_AUTO_BRIGHTNESS           = 0x0075 | _OOC,

  CAMERA_ACCESS_ENABLED                 = 0x0076 | _OOC,
  CAMERA_ACCESS_DISABLED                = 0x0077 | _OOC,
  CAMERA_ACCESS_TOGGLE                  = 0x0078 | _OOC,
  
  KEYBOARD_BRIGHTNESS_INC               = 0x0079 | _OSC,
  KEYBOARD_BRIGHTNESS_DEC               = 0x007A | _OSC,
  // KEYBOARD_BACKLIGHT_SET_LEVEL          = 0x007B | _LC,
  KEYBOARD_BACKLIGHT_OOC                = 0x007C | _OOC,
  KEYBOARD_BACKLIGHT_SET_MIN            = 0x007D | _OSC,
  KEYBOARD_BACKLIGHT_SET_MAX            = 0x007E | _OSC,
  KEYBOARD_BACKLIGHT_AUTO               = 0x007F | _OOC,

  // Selection Controls
  ASSIGN_SELECTION                      = 0x0081 | _OSC,
  MODE_STEP                             = 0x0082 | _OSC,
  RECALL_LAST                           = 0x0083 | _OSC,
  ENTER_CHANNEL                         = 0x0084 | _OSC,
  ORDER_MOVIE                           = 0x0085 | _OSC,
  // CHANNEL                               = 0x0086 | _LC,
  QUIT                                  = 0x0094 | _OSC,
  HELP                                  = 0x0095 | _OOC,
  CHANNEL_INC                           = 0x009C | _OSC,
  CHANNEL_DEC                           = 0x009D | _OSC,
  VCR_PLUS                              = 0x00A0 | _OSC,
  ONCE                                  = 0x00A1 | _OSC,
  DAILY                                 = 0x00A2 | _OSC,
  WEEKLY                                = 0x00A3 | _OSC,
  MONTHLY                               = 0x00A4 | _OSC,

  // Transport Controls
  PLAY                                  = 0x00B0 | _OOC,
  PAUSE                                 = 0x00B1 | _OOC,
  RECORD                                = 0x00B2 | _OOC,
  FAST_FORWARD                          = 0x00B3 | _OOC,
  REWIND                                = 0x00B4 | _OOC,
  SCAN_NEXT_TRACK                       = 0x00B5 | _OSC,
  SCAN_PREVIOUS_TRACK                   = 0x00B6 | _OSC,
  STOP                                  = 0x00B7 | _OSC,
  EJECT                                 = 0x00B8 | _OSC,
  RANDOM_PLAY                           = 0x00B9 | _OOC,
  ENTER_DISC                            = 0x00BB | _MC,
  REPEAT                                = 0x00BC | _OSC,
  // TRACKING                              = 0x00BD | _LC,
  TRACK_NORMAL                          = 0x00BE | _OSC,
  // SLOW_TRACKING                         = 0x00BF | _LC,
  FRAME_FORWARD                         = 0x00C0 | _RTC,
  FRAME_BACK                            = 0x00C1 | _RTC,
  TRACKING_INC                          = 0x00CA | _RTC,
  TRACKING_DEC                          = 0x00CB | _RTC,
  STOP_EJECT                            = 0x00CC | _OSC,
  PLAY_PAUSE                            = 0x00CD | _OSC,
  PLAY_SKIP                             = 0x00CE | _OSC,

  // Search Controls
  MARK                                  = 0x00C2 | _OSC,
  CLEAR_MARK                            = 0x00C3 | _OSC,
  REPEAT_FROM_MARK                      = 0x00C4 | _OOC,
  RETURN_TO_MARK                        = 0x00C5 | _OSC,
  SEARCH_MARK_FORWARD                   = 0x00C6 | _OSC,
  SEARCH_MARK_BACKWARDS                 = 0x00C7 | _OSC,
  COUNTER_RESET                         = 0x00C8 | _OSC,
  SHOW_COUNTER                          = 0x00C9 | _OSC,

  // Audio Controls
  // VOLUME                                = 0x00E0 | _LC,
  // BALANCE                               = 0x00E1 | _LC,
  MUTE                                  = 0x00E2 | _OOC,
  // BASS                                  = 0x00E3 | _LC,
  // TREBLE                                = 0x00E4 | _LC,
  BASS_BOOST                            = 0x00E5 | _OOC,
  SURROUND_MODE                         = 0x00E6 | _OSC,
  LOUDNESS                              = 0x00E7 | _OOC,
  MPX                                   = 0x00E8 | _OOC,
  VOLUME_INC                            = 0x00E9 | _RTC,
  VOLUME_DEC                            = 0x00EA | _RTC,

  // Speed Controls
  SPEED_SELECT                          = 0x00F0 | _OSC,
  SLOW                                  = 0x00F5 | _OSC,

  // Home and Security Controls
  FAN_ENABLE                            = 0x0100 | _OOC,
  // FAN_SPEED                             = 0x0101 | _LC,
  LIGHT_ENABLE                          = 0x0102 | _OOC,
  // LIGHT_ILLUMINATION_LEVEL              = 0x0103 | _LC,
  CLIMATE_CONTROL_ENABLE                = 0x0104 | _OOC,
  // ROOM_TEMPERATURE                      = 0x0105 | _LC,
  SECURITY_ENABLE                       = 0x0106 | _OOC,
  FIRE_ALARM                            = 0x0107 | _OSC,
  POLICE_ALARM                          = 0x0108 | _OSC,
  // PROXIMITY                             = 0x0109 | _LC,
  MOTION                                = 0x010A | _OSC,
  DURESS_ALARM                          = 0x010B | _OSC,
  HOLDUP_ALARM                          = 0x010C | _OSC,
  MEDICAL_ALARM                         = 0x010D | _OSC,

  // Speaker Channels
  BALANCE_RIGHT                         = 0x0150 | _RTC,
  BALANCE_LEFT                          = 0x0151 | _RTC,
  BASS_INC                              = 0x0152 | _RTC,
  BASS_DEC                              = 0x0153 | _RTC,
  TREBLE_INC                            = 0x0154 | _RTC,
  TREBLE_DEC                            = 0x0155 | _RTC,

  // PC Theatre
  // SUB_CHANNEL                           = 0x0170 | _LC,
  SUB_CHANNEL_INC                       = 0x0171 | _OSC,
  SUB_CHANNEL_DEC                       = 0x0172 | _OSC,
  ALTERNATE_AUDIO_INC                   = 0x0173 | _OSC,
  ALTERNATE_AUDIO_DEC                   = 0x0174 | _OSC,

  // Application Launch
  AL_LAUNCH_BUTTON_CONFIGURATION_TOOL   = 0x0181 | _OSC,  // SEL
  AL_PROGRAMMABLE_BUTTON_CONFIGURATION  = 0x0182 | _OSC,  // SEL
  AL_CONSUMER_CONTROL_CONFIGURATION     = 0x0183 | _OSC,  // SEL
  AL_WORD_PROCESSOR                     = 0x0184 | _OSC,  // SEL
  AL_TEXT_EDITOR                        = 0x0185 | _OSC,  // SEL
  AL_SPREADSHEET                        = 0x0186 | _OSC,  // SEL
  AL_GRAPHICS_EDITOR                    = 0x0187 | _OSC,  // SEL
  AL_PRESENTATION_APP                   = 0x0188 | _OSC,  // SEL
  AL_DATABASE_APP                       = 0x0189 | _OSC,  // SEL
  AL_EMAIL_READER                       = 0x018A | _OSC,  // SEL
  AL_NEWSREADER                         = 0x018B | _OSC,  // SEL
  AL_VOICEMAIL                          = 0x018C | _OSC,  // SEL
  AL_CONTACTS_ADDRESS_BOOK              = 0x018D | _OSC,  // SEL
  AL_CALENDAR_SCHEDULE                  = 0x018E | _OSC,  // SEL
  AL_TASK_PROJECT_MANAGER               = 0x018F | _OSC,  // SEL
  AL_LOG_JOURNAL_TIMECARD               = 0x0190 | _OSC,  // SEL
  AL_CHECKBOOK_FINANCE                  = 0x0191 | _OSC,  // SEL
  AL_CALCULATOR                         = 0x0192 | _OSC,  // SEL
  AL_AV_CAPTURE_PLAYBACK                = 0x0193 | _OSC,  // SEL
  AL_LOCAL_MACHINE_BROWSER              = 0x0194 | _OSC,  // SEL
  AL_LAN_WAN_BROWSER                    = 0x0195 | _OSC,  // SEL
  AL_INTERNET_BROWSER                   = 0x0196 | _OSC,  // SEL
  AL_REMOTE_NETWORKING_ISP_CONNECT      = 0x0197 | _OSC,  // SEL
  AL_NETWORK_CONFERENCE                 = 0x0198 | _OSC,  // SEL
  AL_NETWORK_CHAT                       = 0x0199 | _OSC,  // SEL
  AL_TELEPHONY_DIALER                   = 0x019A | _OSC,  // SEL
  AL_LOGON                              = 0x019B | _OSC,  // SEL
  AL_LOGOFF                             = 0x019C | _OSC,  // SEL
  AL_LOGON_LOGOFF                       = 0x019D | _OSC,  // SEL
  AL_TERMINAL_LOCK_SCREENSAVER          = 0x019E | _OSC,  // SEL
  AL_CONTROL_PANEL                      = 0x019F | _OSC,  // SEL
  AL_COMMAND_LINE_PROCESSOR_RUN         = 0x01A0 | _OSC,  // SEL
  AL_PROCESS_TASK_MANAGER               = 0x01A1 | _OSC,  // SEL
  AL_SELECT_TASK_APPLICATION            = 0x01A2 | _OSC,  // SEL
  AL_NEXT_TASK_APPLICATION              = 0x01A3 | _OSC,  // SEL
  AL_PREVIOUS_TASK_APPLICATION          = 0x01A4 | _OSC,  // SEL
  AL_PREEMPTIVE_HALT_TASK_APPLICATION   = 0x01A5 | _OSC,  // SEL
  AL_INTEGRATED_HELP_CENTER             = 0x01A6 | _OSC,  // SEL
  AL_DOCUMENTS                          = 0x01A7 | _OSC,  // SEL
  AL_THESAURUS                          = 0x01A8 | _OSC,  // SEL
  AL_DICTIONARY                         = 0x01A9 | _OSC,  // SEL
  AL_DESKTOP                            = 0x01AA | _OSC,  // SEL
  AL_SPELL_CHECK                        = 0x01AB | _OSC,  // SEL
  AL_GRAMMAR_CHECK                      = 0x01AC | _OSC,  // SEL
  AL_WIRELESS_STATUS                    = 0x01AD | _OSC,  // SEL
  AL_KEYBOARD_LAYOUT                    = 0x01AE | _OSC,  // SEL
  AL_VIRUS_PROTECTION                   = 0x01AF | _OSC,  // SEL
  AL_ENCRYPTION                         = 0x01B0 | _OSC,  // SEL
  AL_SCREEN_SAVER                       = 0x01B1 | _OSC,  // SEL
  AL_ALARMS                             = 0x01B2 | _OSC,  // SEL
  AL_CLOCK                              = 0x01B3 | _OSC,  // SEL
  AL_FILE_BROWSER                       = 0x01B4 | _OSC,  // SEL
  AL_POWER_STATUS                       = 0x01B5 | _OSC,  // SEL
  AL_IMAGE_BROWSER                      = 0x01B6 | _OSC,  // SEL
  AL_AUDIO_BROWSER                      = 0x01B7 | _OSC,  // SEL
  AL_MOVIE_BROWSER                      = 0x01B8 | _OSC,  // SEL
  AL_DIGITAL_RIGHTS_MANAGER             = 0x01B9 | _OSC,  // SEL
  AL_DIGITAL_WALLET                     = 0x01BA | _OSC,  // SEL
  AL_INSTANT_MESSAGING                  = 0x01BC | _OSC,  // SEL
  AL_OEM_FEATURES_TIPS_TUTORIAL_BROWSER = 0x01BD | _OSC,  // SEL
  AL_OEM_HELP                           = 0x01BE | _OSC,  // SEL
  AL_ONLINE_COMMUNITY                   = 0x01BF | _OSC,  // SEL
  AL_ENTERTAINMENT_CONTENT_BROWSER      = 0x01C0 | _OSC,  // SEL
  AL_ONLINE_SHOPPING_BROWSER            = 0x01C1 | _OSC,  // SEL
  AL_SMARTCARD_INFORMATION_HELP         = 0x01C2 | _OSC,  // SEL
  AL_MARKET_MONITOR_FINANCE_BROWSER     = 0x01C3 | _OSC,  // SEL
  AL_CUSTOMIZED_CORPORATE_NEWS_BROWSER  = 0x01C4 | _OSC,  // SEL
  AL_ONLINE_ACTIVITY_BROWSER            = 0x01C5 | _OSC,  // SEL
  AL_RESEARCH_SEARCH_BROWSER            = 0x01C6 | _OSC,  // SEL
  AL_AUDIO_PLAYER                       = 0x01C7 | _OSC,  // SEL
  AL_MESSAGE_STATUS                     = 0x01C8 | _OSC,  // SEL
  AL_CONTACT_SYNC                       = 0x01C9 | _OSC,  // SEL
  AL_NAVIGATION                         = 0x01CA | _OSC,  // SEL
  AL_CONTEXT_AWARE_DESKTOP_ASSISTANT    = 0x01CB | _OSC,  // SEL

  // Generic GUI Application Controls
  AC_NEW                                = 0x0201 | _OSC,  // SEL
  AC_OPEN                               = 0x0202 | _OSC,  // SEL
  AC_CLOSE                              = 0x0203 | _OSC,  // SEL
  AC_EXIT                               = 0x0204 | _OSC,  // SEL
  AC_MAXIMIZE                           = 0x0205 | _OSC,  // SEL
  AC_MINIMIZE                           = 0x0206 | _OSC,  // SEL
  AC_SAVE                               = 0x0207 | _OSC,  // SEL
  AC_PRINT                              = 0x0208 | _OSC,  // SEL
  AC_PROPERTIES                         = 0x0209 | _OSC,  // SEL
  AC_UNDO                               = 0x021A | _OSC,  // SEL
  AC_COPY                               = 0x021B | _OSC,  // SEL
  AC_CUT                                = 0x021C | _OSC,  // SEL
  AC_PASTE                              = 0x021D | _OSC,  // SEL
  AC_SELECT_ALL                         = 0x021E | _OSC,  // SEL
  AC_FIND                               = 0x021F | _OSC,  // SEL
  AC_FIND_AND_REPLACE                   = 0x0220 | _OSC,  // SEL
  AC_SEARCH                             = 0x0221 | _OSC,  // SEL
  AC_GO_TO                              = 0x0222 | _OSC,  // SEL
  AC_HOME                               = 0x0223 | _OSC,  // SEL
  AC_BACK                               = 0x0224 | _OSC,  // SEL
  AC_FORWARD                            = 0x0225 | _OSC,  // SEL
  AC_STOP                               = 0x0226 | _OSC,  // SEL
  AC_REFRESH                            = 0x0227 | _OSC,  // SEL
  AC_PREVIOUS_LINK                      = 0x0228 | _OSC,  // SEL
  AC_NEXT_LINK                          = 0x0229 | _OSC,  // SEL
  AC_BOOKMARKS                          = 0x022A | _OSC,  // SEL
  AC_HISTORY                            = 0x022B | _OSC,  // SEL
  AC_SUBSCRIPTIONS                      = 0x022C | _OSC,  // SEL
  AC_ZOOM_IN                            = 0x022D | _OSC,  // SEL
  AC_ZOOM_OUT                           = 0x022E | _OSC,  // SEL
  // AC_ZOOM                               = 0x022F | _LC,
  AC_FULL_SCREEN_VIEW                   = 0x0230 | _OSC,  // SEL
  AC_NORMAL_VIEW                        = 0x0231 | _OSC,  // SEL
  AC_VIEW_TOGGLE                        = 0x0232 | _OSC,  // SEL
  AC_SCROLL_UP                          = 0x0233 | _OSC,  // SEL
  AC_SCROLL_DOWN                        = 0x0234 | _OSC,  // SEL
  // AC_SCROLL                             = 0x0235 | _LC,
  AC_PAN_LEFT                           = 0x0236 | _OSC,  // SEL
  AC_PAN_RIGHT                          = 0x0237 | _OSC,  // SEL
  // AC_PAN                                = 0x0238 | _LC,
  AC_NEW_WINDOW                         = 0x0239 | _OSC,  // SEL
  AC_TILE_HORIZONTALLY                  = 0x023A | _OSC,  // SEL
  AC_TILE_VERTICALLY                    = 0x023B | _OSC,  // SEL
  AC_FORMAT                             = 0x023C | _OSC,  // SEL
  AC_EDIT                               = 0x023D | _OSC,  // SEL
  AC_BOLD                               = 0x023E | _OSC,  // SEL
  AC_ITALICS                            = 0x023F | _OSC,  // SEL
  AC_UNDERLINE                          = 0x0240 | _OSC,  // SEL
  AC_STRIKETHROUGH                      = 0x0241 | _OSC,  // SEL
  AC_SUBSCRIPT                          = 0x0242 | _OSC,  // SEL
  AC_SUPERSCRIPT                        = 0x0243 | _OSC,  // SEL
  AC_ALL_CAPS                           = 0x0244 | _OSC,  // SEL
  AC_ROTATE                             = 0x0245 | _OSC,  // SEL
  AC_RESIZE                             = 0x0246 | _OSC,  // SEL
  AC_FLIP_HORIZONTAL                    = 0x0247 | _OSC,  // SEL
  AC_FLIP_VERTICAL                      = 0x0248 | _OSC,  // SEL
  AC_MIRROR_HORIZONTAL                  = 0x0249 | _OSC,  // SEL
  AC_MIRROR_VERTICAL                    = 0x024A | _OSC,  // SEL
  AC_FONT_SELECT                        = 0x024B | _OSC,  // SEL
  AC_FONT_COLOR                         = 0x024C | _OSC,  // SEL
  AC_FONT_SIZE                          = 0x024D | _OSC,  // SEL
  AC_JUSTIFY_LEFT                       = 0x024E | _OSC,  // SEL
  AC_JUSTIFY_CENTER_H                   = 0x024F | _OSC,  // SEL
  AC_JUSTIFY_RIGHT                      = 0x0250 | _OSC,  // SEL
  AC_JUSTIFY_BLOCK_H                    = 0x0251 | _OSC,  // SEL
  AC_JUSTIFY_TOP                        = 0x0252 | _OSC,  // SEL
  AC_JUSTIFY_CENTER_V                   = 0x0253 | _OSC,  // SEL
  AC_JUSTIFY_BOTTOM                     = 0x0254 | _OSC,  // SEL
  AC_JUSTIFY_BLOCK_V                    = 0x0255 | _OSC,  // SEL
  AC_INDENT_DECREASE                    = 0x0256 | _OSC,  // SEL
  AC_INDENT_INCREASE                    = 0x0257 | _OSC,  // SEL
  AC_NUMBERED_LIST                      = 0x0258 | _OSC,  // SEL
  AC_RESTART_NUMBERING                  = 0x0259 | _OSC,  // SEL
  AC_BULLETED_LIST                      = 0x025A | _OSC,  // SEL
  AC_PROMOTE                            = 0x025B | _OSC,  // SEL
  AC_DEMOTE                             = 0x025C | _OSC,  // SEL
  AC_YES                                = 0x025D | _OSC,  // SEL
  AC_NO                                 = 0x025E | _OSC,  // SEL
  AC_CANCEL                             = 0x025F | _OSC,  // SEL
  AC_CATALOG                            = 0x0260 | _OSC,  // SEL
  AC_BUY_CHECKOUT                       = 0x0261 | _OSC,  // SEL
  AC_ADD_TO_CART                        = 0x0262 | _OSC,  // SEL
  AC_EXPAND                             = 0x0263 | _OSC,  // SEL
  AC_EXPAND_ALL                         = 0x0264 | _OSC,  // SEL
  AC_COLLAPSE                           = 0x0265 | _OSC,  // SEL
  AC_COLLAPSE_ALL                       = 0x0266 | _OSC,  // SEL
  AC_PRINT_PREVIEW                      = 0x0267 | _OSC,  // SEL
  AC_PASTE_SPECIAL                      = 0x0268 | _OSC,  // SEL
  AC_INSERT_MODE                        = 0x0269 | _OSC,  // SEL
  AC_DELETE                             = 0x026A | _OSC,  // SEL
  AC_LOCK                               = 0x026B | _OSC,  // SEL
  AC_UNLOCK                             = 0x026C | _OSC,  // SEL
  AC_PROTECT                            = 0x026D | _OSC,  // SEL
  AC_UNPROTECT                          = 0x026E | _OSC,  // SEL
  AC_ATTACH_COMMENT                     = 0x026F | _OSC,  // SEL
  AC_DELETE_COMMENT                     = 0x0270 | _OSC,  // SEL
  AC_VIEW_COMMENT                       = 0x0271 | _OSC,  // SEL
  AC_SELECT_WORD                        = 0x0272 | _OSC,  // SEL
  AC_SELECT_SENTENCE                    = 0x0273 | _OSC,  // SEL
  AC_SELECT_PARAGRAPH                   = 0x0274 | _OSC,  // SEL
  AC_SELECT_COLUMN                      = 0x0275 | _OSC,  // SEL
  AC_SELECT_ROW                         = 0x0276 | _OSC,  // SEL
  AC_SELECT_TABLE                       = 0x0277 | _OSC,  // SEL
  AC_SELECT_OBJECT                      = 0x0278 | _OSC,  // SEL
  AC_REDO_REPEAT                        = 0x0279 | _OSC,  // SEL
  AC_SORT                               = 0x027A | _OSC,  // SEL
  AC_SORT_ASCENDING                     = 0x027B | _OSC,  // SEL
  AC_SORT_DESCENDING                    = 0x027C | _OSC,  // SEL
  AC_FILTER                             = 0x027D | _OSC,  // SEL
  AC_SET_CLOCK                          = 0x027E | _OSC,  // SEL
  AC_VIEW_CLOCK                         = 0x027F | _OSC,  // SEL
  AC_SELECT_TIME_ZONE                   = 0x0280 | _OSC,  // SEL
  AC_EDIT_TIME_ZONES                    = 0x0281 | _OSC,  // SEL
  AC_SET_ALARM                          = 0x0282 | _OSC,  // SEL
  AC_CLEAR_ALARM                        = 0x0283 | _OSC,  // SEL
  AC_SNOOZE_ALARM                       = 0x0284 | _OSC,  // SEL
  AC_RESET_ALARM                        = 0x0285 | _OSC,  // SEL
  AC_SYNCHRONIZE                        = 0x0286 | _OSC,  // SEL
  AC_SEND_RECEIVE                       = 0x0287 | _OSC,  // SEL
  AC_SEND_TO                            = 0x0288 | _OSC,  // SEL
  AC_REPLY                              = 0x0289 | _OSC,  // SEL
  AC_REPLY_ALL                          = 0x028A | _OSC,  // SEL
  AC_FORWARD_MSG                        = 0x028B | _OSC,  // SEL
  AC_SEND                               = 0x028C | _OSC,  // SEL
  AC_ATTACH_FILE                        = 0x028D | _OSC,  // SEL
  AC_UPLOAD                             = 0x028E | _OSC,  // SEL
  AC_DOWNLOAD                           = 0x028F | _OSC,  // SEL
  AC_SET_BORDERS                        = 0x0290 | _OSC,  // SEL
  AC_INSERT_ROW                         = 0x0291 | _OSC,  // SEL
  AC_INSERT_COLUMN                      = 0x0292 | _OSC,  // SEL
  AC_INSERT_FILE                        = 0x0293 | _OSC,  // SEL
  AC_INSERT_PICTURE                     = 0x0294 | _OSC,  // SEL
  AC_INSERT_OBJECT                      = 0x0295 | _OSC,  // SEL
  AC_INSERT_SYMBOL                      = 0x0296 | _OSC,  // SEL
  AC_SAVE_AND_CLOSE                     = 0x0297 | _OSC,  // SEL
  AC_RENAME                             = 0x0298 | _OSC,  // SEL
  AC_MERGE                              = 0x0299 | _OSC,  // SEL
  AC_SPLIT                              = 0x029A | _OSC,  // SEL
  AC_DISRIBUTE_HORIZONTALLY             = 0x029B | _OSC,  // SEL
  AC_DISTRIBUTE_VERTICALLY              = 0x029C | _OSC,  // SEL
  AC_NEXT_KEYBOARD_LAYOUT_SELECT        = 0x029D | _OSC,  // SEL
  AC_NAVIGATION_GUIDANCE                = 0x029E | _OSC,  // SEL
  AC_DESKTOP_SHOW_ALL_WINDOWS           = 0x029F | _OSC,  // SEL
  AC_SOFT_KEY_LEFT                      = 0x02A0 | _OSC,  // SEL
  AC_SOFT_KEY_RIGHT                     = 0x02A1 | _OSC,  // SEL
  AC_DESKTOP_SHOW_ALL_APPLICATIONS      = 0x02A2 | _OSC,  // SEL
  AC_IDLE_KEEP_ALIVE                    = 0x02B0 | _OSC,  // SEL

  // Input Assist Selectors
  KEYBOARD_INPUT_ASSIST_PREVIOUS        = 0x02C7 | _OSC,  // SEL
  KEYBOARD_INPUT_ASSIST_NEXT            = 0x02C8 | _OSC,  // SEL
  KEYBOARD_INPUT_ASSIST_PREVIOUS_GROUP  = 0x02C9 | _OSC,  // SEL
  KEYBOARD_INPUT_ASSIST_NEXT_GROUP      = 0x02CA | _OSC,  // SEL
  KEYBOARD_INPUT_ASSIST_ACCEPT          = 0x02CB | _OSC,  // SEL
  KEYBOARD_INPUT_ASSIST_CANCEL          = 0x02CC | _OSC,  // SEL

  PRIVACY_SCREEN_TOGGLE                 = 0x02D0 | _OOC,
  PRIVACY_SCREEN_LEVEL_DEC              = 0x02D1 | _RTC,
  PRIVACY_SCREEN_LEVEL_INC              = 0x02D2 | _RTC,
  PRIVACY_SCREEN_LEVEL_MINIMUM          = 0x02D3 | _OSC,
  PRIVACY_SCREEN_LEVEL_MAXIMUM          = 0x02D4 | _OSC,

  CONTACT_EDITED                        = 0x0500 | _OOC,
  CONTACT_ADDED                         = 0x0501 | _OOC,
  CONTACT_RECORD_ACTIVE                 = 0x0502 | _OOC,
};

// clang-format on

}  // namespace midi2usb
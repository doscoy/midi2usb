#pragma once

#include "reportdefs.hpp"
#include "reportmaker.hpp"

namespace midi2usb {

namespace detail {

template <class FuncType, FuncType... Funcs>
class ChFunctionTable {
  static_assert(sizeof...(Funcs) <= 16);

 public:
  constexpr ChFunctionTable() : table_{Funcs...} {}

  template <class... Args>
  void Call(uint8_t ch, Args... args) const {
    if (0 < ch && ch <= sizeof...(Funcs)) table_[ch - 1](args...);
  }

 private:
  FuncType table_[sizeof...(Funcs)];
};

template <size_t N>
class StrListImpl {
 public:
  template <size_t... Len>
  constexpr StrListImpl(const char (&... str)[Len]) : list_{str...} {}
  constexpr const char* operator[](size_t i) const { return list_[i]; }
  constexpr const char* const* begin() const { return list_; }
  constexpr const char* const* end() const { return &list_[N]; }
  static constexpr auto size() { return N; }

 private:
  const char* list_[N];
};

}  // namespace detail

using Note2FuncType = void (*)(ReportMakers* makers, bool on, uint8_t note,
                               uint8_t vel);
using CC2FuncType = void (*)(ReportMakers* makers, uint8_t num, uint8_t val);
using Prog2FuncType = void (*)(ReportMakers* makers, uint8_t num);
using TapFuncType = void (*)(ReportMakers* makers);

void Blank(ReportMakers*, bool, uint8_t, uint8_t) { return; }  // do nothing...
void Blank(ReportMakers*, uint8_t, uint8_t) { return; }        // do nothing...
void Blank(ReportMakers*, uint8_t) { return; }                 // do nothing...
void Blank(ReportMakers*) { return; }                          // do nothing...

using DataModifyFuncType = uint8_t (*)(uint8_t data1, uint8_t data2,
                                       size_t tablesize);

uint8_t NoModify(uint8_t, uint8_t ret, size_t) { return ret; }  // do nothing...

template <size_t... N>
constexpr auto MakeStrList(const char (&... str)[N]) {
  return detail::StrListImpl<sizeof...(N)>{str...};
}

namespace Note2 {

template <Note2FuncType... Funcs>
using FunctionTable = detail::ChFunctionTable<Note2FuncType, Funcs...>;

template <DataModifyFuncType DataModify, Note2FuncType... Funcs>
void Selector(ReportMakers* makers, bool on, uint8_t note, uint8_t vel) {
  constexpr Note2FuncType kTable[]{Funcs...};
  constexpr auto kSize{sizeof(kTable) / sizeof(Note2FuncType)};

  if constexpr (DataModify != nullptr) vel = DataModify(note, vel, kSize);
  kTable[note % kSize](makers, on, note, vel);
}

template <uint8_t Offset, MODKEY... Mods>
void Keyboard(ReportMakers* makers, bool on, uint8_t note, uint8_t) {
  constexpr bool kUseMod{sizeof...(Mods) != 0};

  if (on) {
    if constexpr (kUseMod) makers->keyboard.PushMod(Enum2Num(EnumOR(Mods...)));
    makers->keyboard.PushKey(note + Offset);

  } else {
    if constexpr (kUseMod) makers->keyboard.PopMod(Enum2Num(EnumOR(Mods...)));
    makers->keyboard.PopKey(note + Offset);
  }
}

template <KEY Key, MODKEY... Mods>
void KeyboardKey(ReportMakers* makers, bool on, uint8_t, uint8_t) {
  Keyboard<0x00, Mods...>(makers, on, Enum2Num(Key), 0);
}

template <size_t N, const detail::StrListImpl<N>& StrList>
void StringList(ReportMakers* makers, bool on, uint8_t note, uint8_t) {
  if (on) makers->keyboard.PushStr(StrList[note % StrList.size()]);
}

template <const char* Str>
void String(ReportMakers* makers, bool on, uint8_t, uint8_t) {
  if (on) makers->keyboard.PushStr(Str);
}

template <MOUSEBUTTON Button>
void MouseButton(ReportMakers* makers, bool on, uint8_t, uint8_t) {
  if (on) {
    makers->mouse.PushBtn(Enum2Num(Button));

  } else {
    makers->mouse.PopBtn(Enum2Num(Button));
  }
}

template <MOUSEMOVE Move>
void MouseMove(ReportMakers* makers, bool on, uint8_t, uint8_t vel) {
  constexpr auto kDir{EnumTest(Move, MOUSEMOVE::_NEGATIVE) ? -1 : 1};

  if (on) {
    makers->mouse.PushMove(EnumBIT(Move), vel * kDir);

  } else {
    makers->mouse.PopMove(EnumBIT(Move));
  }
}

template <GAMEPADBUTTON Button>
void GamePadButton(ReportMakers* makers, bool on, uint8_t, uint8_t) {
  if (on) {
    makers->gamepad.PushBtn(Enum2Num(Button));

  } else {
    makers->gamepad.PopBtn(Enum2Num(Button));
  }
}

template <GAMEPADAXIS Axis>
void GamePadAxis(ReportMakers* makers, bool on, uint8_t, uint8_t vel) {
  constexpr auto kDir{EnumTest(Axis, GAMEPADAXIS::_NEGATIVE) ? -1 : 1};

  if (on) {
    makers->gamepad.PushAxis(EnumBIT(Axis), vel * kDir);

  } else {
    makers->gamepad.PopAxis(EnumBIT(Axis));
  }
}

template <CONSUMER ID>
void Consumer(ReportMakers* makers, bool on, uint8_t, uint8_t) {
  constexpr auto kRawID{Enum2Num(EnumAND(ID, CONSUMER::_MASK))};

  if constexpr (EnumTest(ID, CONSUMER::_OSC)) {
    if (on) makers->consumer.Push(kRawID);

  } else {
    if (on) {
      makers->consumer.Push(kRawID);

    } else {
      makers->consumer.Pop();
    }
  }
}

void AppLancher(ReportMakers* makers, bool on, uint8_t note, uint8_t) {
  constexpr uint16_t kAppLancherOffset{0x0180};
  constexpr uint16_t kAppLancherBeg{Enum2Num(
      EnumAND(CONSUMER::AL_LAUNCH_BUTTON_CONFIGURATION_TOOL, CONSUMER::_MASK))};
  constexpr uint16_t kAppLancherEnd{Enum2Num(
      EnumAND(CONSUMER::AL_CONTEXT_AWARE_DESKTOP_ASSISTANT, CONSUMER::_MASK))};
  constexpr uint16_t kNoteMin{kAppLancherBeg - kAppLancherOffset};
  constexpr uint16_t kNoteMax{kAppLancherEnd - kAppLancherOffset};

  if (kNoteMin <= note && note < kNoteMax) {
    if (on) makers->consumer.Push(note + kAppLancherOffset);
  }
}

template <uint8_t Offset>
void AppControll(ReportMakers* makers, bool on, uint8_t note, uint8_t) {
  constexpr uint16_t kAppControllOffset{0x0200 + Offset};
  constexpr uint16_t kAppControllBeg{
      Enum2Num(EnumAND(CONSUMER::AC_NEW, CONSUMER::_MASK))};
  constexpr uint16_t kAppControllEnd{
      Enum2Num(EnumAND(CONSUMER::AC_IDLE_KEEP_ALIVE, CONSUMER::_MASK))};
  constexpr uint16_t kNoteMin{(kAppControllBeg - kAppControllOffset) < 0
                                  ? 0
                                  : kAppControllBeg - kAppControllOffset};
  constexpr uint16_t kNoteMax{(kAppControllEnd - kAppControllOffset) < 0
                                  ? 0
                                  : kAppControllEnd - kAppControllOffset};

  if (kNoteMin <= note && note < kNoteMax) {
    if (on) makers->consumer.Push(note + kAppControllOffset);
  }
}

}  // namespace Note2

namespace CC2 {

template <CC2FuncType... Funcs>
using FunctionTable = detail::ChFunctionTable<CC2FuncType, Funcs...>;

template <DataModifyFuncType DataModify, CC2FuncType... Funcs>
void Selector(ReportMakers* makers, uint8_t num, uint8_t val) {
  constexpr CC2FuncType kTable[]{Funcs...};
  constexpr auto kSize{sizeof(kTable) / sizeof(CC2FuncType)};

  if constexpr (DataModify != nullptr) {
    if (val != 0) val = DataModify(num, val, kSize);
  }
  kTable[num % kSize](makers, num, val);
}

template <uint8_t Offset, MODKEY... Mods>
void Keyboard(ReportMakers* makers, uint8_t num, uint8_t val) {
  Note2::Keyboard<Offset, Mods...>(makers, val > 0, num, 0);
}

template <KEY Key, MODKEY... Mods>
void KeyboardKey(ReportMakers* makers, uint8_t, uint8_t val) {
  Note2::Keyboard<0x00, Mods...>(makers, val > 0, Enum2Num(Key), 0);
}

template <size_t N, const detail::StrListImpl<N>& StrList>
void StringList(ReportMakers* makers, uint8_t num, uint8_t val) {
  Note2::StringList<N, StrList>(makers, val > 0, num, 0);
}

template <const char* Str>
void String(ReportMakers* makers, uint8_t, uint8_t val) {
  Note2::String<Str>(makers, val > 0, 0, 0);
}

template <MOUSEBUTTON Button>
void MouseButton(ReportMakers* makers, uint8_t, uint8_t val) {
  Note2::MouseButton<Button>(makers, val > 0, 0, 0);
}

template <MOUSEMOVE Move>
void MouseMove(ReportMakers* makers, uint8_t, uint8_t val) {
  Note2::MouseMove<Move>(makers, val > 0, 0, val);
}

template <GAMEPADBUTTON Button>
void GamePadButton(ReportMakers* makers, uint8_t, uint8_t val) {
  Note2::GamePadButton<Button>(makers, val > 0, 0, 0);
}

template <GAMEPADAXIS Axis>
void GamePadAxis(ReportMakers* makers, uint8_t, uint8_t val) {
  Note2::GamePadAxis<Axis>(makers, val > 0, 0, val);
}

template <CONSUMER ID>
void Consumer(ReportMakers* makers, uint8_t, uint8_t val) {
  Note2::Consumer<ID>(makers, val > 0, 0, 0);
}

void AppLancher(ReportMakers* makers, uint8_t num, uint8_t val) {
  Note2::AppLancher(makers, val > 0, num, 0);
}

template <uint8_t Offset>
void AppControll(ReportMakers* makers, uint8_t num, uint8_t val) {
  Note2::AppControll<Offset>(makers, val > 0, num, 0);
}

}  // namespace CC2

namespace Prog2 {

template <Prog2FuncType... Funcs>
using FunctionTable = detail::ChFunctionTable<Prog2FuncType, Funcs...>;

template <DataModifyFuncType DataModify, Prog2FuncType... Funcs>
void Selector(ReportMakers* makers, uint8_t num) {
  constexpr Prog2FuncType kTable[]{Funcs...};
  constexpr auto kSize{sizeof(kTable) / sizeof(Prog2FuncType)};

  if constexpr (DataModify != nullptr) {
    kTable[num % kSize](makers, DataModify(num, 127, kSize));

  } else {
    kTable[num % kSize](makers, num);
  }
}

template <uint8_t Offset, MODKEY... Mods>
void Keyboard(ReportMakers* makers, uint8_t num) {
  Note2::Keyboard<Offset, Mods...>(makers, true, num, 0);
  makers->keyboard.SetTap();
}

template <KEY Key, MODKEY... Mods>
void KeyboardKey(ReportMakers* makers, uint8_t) {
  Note2::Keyboard<0x00, Mods...>(makers, true, Enum2Num(Key), 0);
  makers->keyboard.SetTap();
}

template <size_t N, const detail::StrListImpl<N>& StrList>
void StringList(ReportMakers* makers, uint8_t num) {
  Note2::StringList<N, StrList>(makers, true, num, 0);
}

template <const char* Str>
void String(ReportMakers* makers, uint8_t) {
  Note2::String<Str>(makers, true, 0, 0);
}

template <MOUSEBUTTON Button>
void MouseButton(ReportMakers* makers, uint8_t) {
  Note2::MouseButton<Button>(makers, true, 0, 0);
  makers->mouse.SetTap();
}

template <MOUSEMOVE Move>
void MouseMove(ReportMakers* makers, uint8_t num) {
  Note2::MouseMove<Move>(makers, true, 0, num);
  makers->mouse.SetTap();
}

template <GAMEPADBUTTON Button>
void GamePadButton(ReportMakers* makers, uint8_t) {
  Note2::GamePadButton<Button>(makers, true, 0, 0);
  makers->gamepad.SetTap();
}

template <GAMEPADAXIS Axis>
void GamePadAxis(ReportMakers* makers, uint8_t num) {
  Note2::GamePadAxis<Axis>(makers, true, 0, num);
  makers->gamepad.SetTap();
}

template <CONSUMER ID>
void Consumer(ReportMakers* makers, uint8_t) {
  Note2::Consumer<ID>(makers, true, 0, 0);
  if constexpr (not EnumTest(ID, CONSUMER::_OSC)) makers->consumer.SetTap();
}

void AppLancher(ReportMakers* makers, uint8_t num) {
  Note2::AppLancher(makers, true, num, 0);
}

template <uint8_t Offset>
void AppControll(ReportMakers* makers, uint8_t num) {
  Note2::AppControll<Offset>(makers, true, num, 0);
}

}  // namespace Prog2

namespace Tap {

template <KEY Key, MODKEY... Mods>
void KeyboardKey(ReportMakers* makers) {
  Prog2::KeyboardKey<Key, Mods...>(makers, 0);
}

template <size_t N, const detail::StrListImpl<N>& StrList>
void StringList(ReportMakers* makers, uint8_t num) {
  Prog2::StringList<N, StrList>(makers, num);
}

template <const char* Str>
void String(ReportMakers* makers) {
  Prog2::String<Str>(makers, 0);
}

template <MOUSEBUTTON Button>
void MouseButton(ReportMakers* makers) {
  Prog2::MouseButton<Button>(makers, 0);
}

template <MOUSEMOVE Move, uint8_t Value>
void MouseMove(ReportMakers* makers) {
  constexpr uint8_t kFixValue{std::min(Value, static_cast<uint8_t>(127))};
  Prog2::MouseMove<Move>(makers, kFixValue);
}

template <GAMEPADBUTTON Button>
void GamePadButton(ReportMakers* makers) {
  Prog2::GamePadButton<Button>(makers, 0);
}

template <GAMEPADAXIS Axis, uint8_t Value>
void GamePadAxis(ReportMakers* makers) {
  constexpr uint8_t kFixValue{std::min(Value, static_cast<uint8_t>(127))};
  Prog2::GamePadAxis<Axis>(makers, kFixValue);
}

template <CONSUMER ID>
void Consumer(ReportMakers* makers) {
  Prog2::Consumer<ID>(makers, 0);
}

}  // namespace Tap

namespace CC2 {

using RelativeType = int (*)(uint8_t cc_num);

template <RelativeType RelType, TapFuncType Positive, TapFuncType Negative>
void RelativeEncoder(ReportMakers* makers, uint8_t, uint8_t val) {
  const auto dir{RelType(val)};

  if (dir > 0) {
    Positive(makers);

  } else if (dir < 0) {
    Negative(makers);
  }
}

}  // namespace CC2

namespace RelType {

int BinaryOffset(uint8_t n) { return static_cast<int>(n) - 64; }

int TwosComplement(uint8_t n) { return (static_cast<int8_t>(n << 1) >> 1); }

int SignBit(uint8_t n) {
  return (n & 0b01000000) != 0 ? (n & 0b00111111) : -(n & 0b00111111);
}

int InvSignBit(uint8_t n) {
  return (n & 0b01000000) != 0 ? -(n & 0b00111111) : (n & 0b00111111);
}

}  // namespace RelType

}  // namespace midi2usb
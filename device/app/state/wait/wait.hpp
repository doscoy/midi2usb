#pragma once

#include "../../../debug.hpp"
#include "../statebase.hpp"

namespace midi2usb {  // declaration

class WaitState : public BaseState {
  // enum VID : uint16_t { KORG = 0x0944 };
  // enum PID : uint16_t { PADCONTROL = 0x0105 };
  enum VID : uint16_t { KORG = 0xFFFF };
  enum PID : uint16_t { PADCONTROL = 0xFFFF };

 public:
  using BaseState::BaseState;
  void Update(MidiW*) override {}  // do nothing
  void InputImpl(MidiRW* midirw) override;
  void CleanUp() override {}  // do nothing
};

}  // namespace midi2usb

namespace midi2usb {  // implementation

namespace {  // helper

inline uint16_t DecodeConnectCmd(const uint8_t* head) {
  return (head[0] << 14) | (head[1] << 7) | head[2];
}

}  // namespace

void WaitState::InputImpl(MidiRW* midirw) {
  if (midirw->GetType() != midi::MidiType::SystemExclusive) return;

  const auto data{midirw->GetSysExData()};
  if (data[1] != kLocalSysExID) return;
  if (data[2] != LOCALSYSEXCMD::CONNECT) return;

  ids_->vid = DecodeConnectCmd(&data[3]);
  ids_->pid = DecodeConnectCmd(&data[6]);
  MIDI2USBDEBUGPRINTF("connect: vid=0x%04x pid=0x%04x", ids_->vid, ids_->pid)
  
  if (ids_->vid == VID::KORG && ids_->pid == PID::PADCONTROL) {
    MIDI2USBDEBUGPRINT("change padkontrol mode")
    Change(APPSTATE::PADKONTROL, this);
    
  } else {
    MIDI2USBDEBUGPRINT("change common mode")
    Change(APPSTATE::COMMON, this);
  }
}

}  // namespace midi2usb
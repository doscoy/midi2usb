#pragma once

#include <algorithm>
#include <iterator>

#include "../../debug.hpp"
#include "../../midi/midi.hpp"
#include "../../usb/usb.hpp"
#include "reportmaker.hpp"

namespace midi2usb {  // declaration

enum class APPSTATE { WAIT, COMMON, PADKONTROL };

struct IDs{
  uint16_t vid;
  uint16_t pid;
};

class BaseState {
 public:
  BaseState(APPSTATE* state, IDs* ids, ReportBuffers* reports)
      : reports_{reports}, ids_{ids}, state_{state} {}

  virtual void Update(MidiW* midiw) = 0;
  void Input(MidiRW* midirw);
  void WriteReport();

 protected:
  virtual void InputImpl(MidiRW* midirw) = 0;
  virtual void CleanUp() = 0;
  void Change(APPSTATE new_appstate, BaseState* self);

  ReportBuffers* reports_;
  ReportMakers makers_;
  IDs* ids_;

 private:
  void ResetReports();

  APPSTATE* state_;
};

}  // namespace midi2usb

namespace midi2usb {  // declaration

void BaseState::Input(MidiRW* midirw) {
  if (midirw->GetType() == midi::MidiType::SystemExclusive) {
    const auto data{midirw->GetSysExData()};
    if (data[1] == kLocalSysExID && data[2] == LOCALSYSEXCMD::DISCONNECT) {
      ids_->vid = 0;
      ids_->pid = 0;
      MIDI2USBDEBUGPRINT("change wait mode")
      return Change(APPSTATE::WAIT, this);
    }
  } else if (midirw->GetType() == midi::MidiType::SystemReset) {
    ResetReports();
  }

  MIDI2USBDEBUGPRINTF("midi: %02x %02x %02x",
                      midirw->GetType() | midirw->GetCh(), midirw->GetData1(),
                      midirw->GetData2())
  return InputImpl(midirw);
}

void BaseState::WriteReport() {
  makers_.keyboard.Write(&reports_->keyboard);
  makers_.mouse.Write(&reports_->mouse);
  makers_.gamepad.Write(&reports_->gamepad);
  makers_.consumer.Write(&reports_->consumer);
}

void BaseState::ResetReports() {
  const auto cleaner{[](auto* maker, auto* report) {
    maker->Clear();
    maker->Write(report);
  }};
  cleaner(&makers_.keyboard, &reports_->keyboard);
  cleaner(&makers_.mouse, &reports_->mouse);
  cleaner(&makers_.gamepad, &reports_->gamepad);
  cleaner(&makers_.consumer, &reports_->consumer);
}

void BaseState::Change(APPSTATE new_appstate, BaseState* self) {
  ResetReports();
  *state_ = new_appstate;
  self->CleanUp();
}

}  // namespace midi2usb
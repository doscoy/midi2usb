// #define MIDI2USBDEBUG

#include "share/timekeeper.hpp"
#include "app/app.hpp"
#include "debug.hpp"
#include "midi/midi.hpp"
#include "usb/usb.hpp"

namespace midi2usb {

static ReportBuffers reports;
static USBHIDMgr hid_mgr{&reports};
static MidiMgr midi_mgr{&Serial1};
static App app{&reports};
static TimeKeeper<1000> timekeeper;

}  // namespace midi2usb

void setup() {
  midi2usb::hid_mgr.Init();
  midi2usb::midi_mgr.Init();
  MIDI2USBDEBUGSETUP
}

void loop() {
  midi2usb::timekeeper.Keep();

  midi2usb::midi_mgr.UpdateLED();

  {
    auto midiw{midi2usb::midi_mgr.GetWriter()};
    midi2usb::app.Update(&midiw);
  }

  while (midi2usb::midi_mgr.Available()) {
    auto midirw{midi2usb::midi_mgr.GetReadWriter()};
    midi2usb::app.Input(&midirw);
  }

  midi2usb::app.WriteReport();

  midi2usb::hid_mgr.Task();
}

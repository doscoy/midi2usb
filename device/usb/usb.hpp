#pragma once

#include <Adafruit_TinyUSB.h>

#include "../debug.hpp"
#include "report.hpp"
#include "ringbuffer.hpp"
#include "share/led.hpp"
#include "share/timekeeper.hpp"

namespace midi2usb {  // declaration

struct ReportBuffers {
  using KeyboarReportBuffer = RingBuffer<KeyboarReport, 8>;
  using MouseReportBuffer = RingBuffer<MouseReport, 8>;
  using GamePadReportBuffer = RingBuffer<GamePadReport, 8>;
  using ConsumerReportBuffer = RingBuffer<ConsumerReport, 8>;
  KeyboarReportBuffer keyboard;
  MouseReportBuffer mouse;
  GamePadReportBuffer gamepad;
  ConsumerReportBuffer consumer;
};

class USBHIDMgr {
  static constexpr uint8_t kPollIntervalms{2};
  using MicroSec = decltype(micros());
  using TxLED = LEDFlasher<PIN_LED_TXL, 100>;
  using RxLED = LEDFlasher<PIN_LED_RXL, 100>;

 public:
  USBHIDMgr(ReportBuffers* reports) : reports_{reports} {}

  void Init();
  bool IsSuspended() const { return USBDevice.suspended(); }
  void Task();

 private:
  template <class RingBuffer>
  bool Send(RingBuffer* buf);

  Adafruit_USBD_HID hid_;
  TimeKeeper<kPollIntervalms * 2 * 1000> timekeeper_;
  int priority_;
  TxLED tx_led_;
  RxLED rx_led_;
  ReportBuffers* reports_;
};

}  // namespace midi2usb

namespace midi2usb {  // implementation (template)

template <class RingBuffer>
bool USBHIDMgr::Send(RingBuffer* buf) {
  if (not buf->Available() || not hid_.ready()) return false;

  const auto* data{&buf->Peak()};
  timekeeper_.Keep();
  if (hid_.sendReport(data->kID, data, data->kReportLen)) {
    tx_led_.Flash();
    MIDI2USBDEBUGPRINTREPORT(data->kID, data, data->kReportLen)
    priority_ = (priority_ + 1) & 0b11;
    buf->IncReadHead();
    return true;

  } else {
    return false;
  }
}

}  // namespace midi2usb

namespace midi2usb {  // implementation

void USBHIDMgr::Init() {
  USBDevice.setID(0xDEAD, 0xBEEF);
  USBDevice.setManufacturerDescriptor("doscoy homebrew");
  USBDevice.setProductDescriptor("midi2usb");
  hid_.setPollInterval(kPollIntervalms);
  hid_.setReportDescriptor(kReportDiscriptor, sizeof(kReportDiscriptor));
  hid_.begin();
  while (not USBDevice.mounted()) delay(1);
}

void USBHIDMgr::Task() {
  tx_led_.Update();
  rx_led_.Update();

  switch (priority_) {
    case 0:
      if (Send(&reports_->keyboard)) break;
      if (Send(&reports_->mouse)) break;
      if (Send(&reports_->gamepad)) break;
      if (Send(&reports_->consumer)) break;
      break;
    case 1:
      if (Send(&reports_->mouse)) break;
      if (Send(&reports_->gamepad)) break;
      if (Send(&reports_->consumer)) break;
      if (Send(&reports_->keyboard)) break;
      break;
    case 2:
      if (Send(&reports_->gamepad)) break;
      if (Send(&reports_->consumer)) break;
      if (Send(&reports_->keyboard)) break;
      if (Send(&reports_->mouse)) break;
    case 3:
      if (Send(&reports_->consumer)) break;
      if (Send(&reports_->gamepad)) break;
      if (Send(&reports_->keyboard)) break;
      if (Send(&reports_->mouse)) break;
      break;
  }
}

}  // namespace midi2usb
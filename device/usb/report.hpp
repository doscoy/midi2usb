#pragma once

#include <Adafruit_TinyUSB.h>

namespace midi2usb {

struct KeyboarReport {
  static constexpr uint8_t kID{1};
  static constexpr uint8_t kReportLen{8};
  uint8_t modifier{0};
  uint8_t reserved{0};
  uint8_t keycode[6]{0, 0, 0, 0, 0, 0};
};

struct MouseReport {
  static constexpr uint8_t kID{2};
  static constexpr uint8_t kReportLen{5};
  uint8_t button{0};
  int8_t move[4]{
      0,  // x
      0,  // y
      0,  // wheel
      0   // pan
  };
};

struct GamePadReport {
  static constexpr uint8_t kID{3};
  static constexpr uint8_t kReportLen{6};
  static constexpr uint8_t kAxisNeutral{127};
  uint16_t button{0};
  uint8_t axis[4]{
      kAxisNeutral,  // x
      kAxisNeutral,  // y
      kAxisNeutral,  // z
      kAxisNeutral   // rz
  };
};

struct ConsumerReport {
  static constexpr uint8_t kID{4};
  static constexpr uint8_t kReportLen{2};
  uint16_t id{0};
};

// clang-format off
inline static constexpr uint8_t kReportDiscriptor[]{
    // keyboard
    HID_USAGE_PAGE (HID_USAGE_PAGE_DESKTOP),
    HID_USAGE      (HID_USAGE_DESKTOP_KEYBOARD),
    HID_COLLECTION (HID_COLLECTION_APPLICATION),
      HID_REPORT_ID  (KeyboarReport::kID)
      // 8 bits Modifier Keys (Shfit, Control, Alt, Gui) 
      HID_USAGE_PAGE (HID_USAGE_PAGE_KEYBOARD),
        HID_USAGE_MIN    (224),
        HID_USAGE_MAX    (231),
        HID_LOGICAL_MIN  (0),
        HID_LOGICAL_MAX  (1),
        HID_REPORT_COUNT (8),
        HID_REPORT_SIZE  (1),
        HID_INPUT        (HID_DATA | HID_VARIABLE | HID_ABSOLUTE),
      // 8 bit reserved 
        HID_REPORT_COUNT (1),
        HID_REPORT_SIZE  (8),
        HID_INPUT        (HID_CONSTANT),
      // 6-byte Keycodes 
      HID_USAGE_PAGE (HID_USAGE_PAGE_KEYBOARD),
        HID_USAGE_MIN    (0),
        HID_USAGE_MAX    (255),
        HID_LOGICAL_MIN  (0),
        HID_LOGICAL_MAX  (255),
        HID_REPORT_COUNT (6),
        HID_REPORT_SIZE  (8),
        HID_INPUT        (HID_DATA | HID_ARRAY | HID_ABSOLUTE),
      // 5-bit LED Indicator Kana | Compose | ScrollLock | CapsLock | NumLock 
      HID_USAGE_PAGE  (HID_USAGE_PAGE_LED),
        HID_USAGE_MIN    (1),
        HID_USAGE_MAX    (5),
        HID_REPORT_COUNT (5),
        HID_REPORT_SIZE  (1),
        HID_OUTPUT       (HID_DATA | HID_VARIABLE | HID_ABSOLUTE),
        // led padding 
        HID_REPORT_COUNT (1),
        HID_REPORT_SIZE  (3),
        HID_OUTPUT       (HID_CONSTANT),
    HID_COLLECTION_END,
    // mouse
    HID_USAGE_PAGE (HID_USAGE_PAGE_DESKTOP),
    HID_USAGE      (HID_USAGE_DESKTOP_MOUSE),
    HID_COLLECTION (HID_COLLECTION_APPLICATION),
      HID_REPORT_ID  (MouseReport::kID)
      HID_USAGE      (HID_USAGE_DESKTOP_POINTER),
      HID_COLLECTION (HID_COLLECTION_PHYSICAL),
        HID_USAGE_PAGE (HID_USAGE_PAGE_BUTTON),
          HID_USAGE_MIN   (1),
          HID_USAGE_MAX   (5),
          HID_LOGICAL_MIN (0),
          HID_LOGICAL_MAX (1),
          // Left, Right, Middle, Backward, Forward buttons
          HID_REPORT_COUNT(5),
          HID_REPORT_SIZE (1),
          HID_INPUT       (HID_DATA | HID_VARIABLE | HID_ABSOLUTE),
          // 3 bit padding
          HID_REPORT_COUNT(1),
          HID_REPORT_SIZE (3),
          HID_INPUT       (HID_CONSTANT),
        HID_USAGE_PAGE (HID_USAGE_PAGE_DESKTOP),
          // X, Y position [-127, 127]
          HID_USAGE        (HID_USAGE_DESKTOP_X),
          HID_USAGE        (HID_USAGE_DESKTOP_Y),
          HID_LOGICAL_MIN  (0x81),
          HID_LOGICAL_MAX  (0x7F),
          HID_REPORT_COUNT (2),
          HID_REPORT_SIZE  (8),
          HID_INPUT        (HID_DATA | HID_VARIABLE | HID_RELATIVE),
          // Verital wheel scroll [-127, 127]
          HID_USAGE        (HID_USAGE_DESKTOP_WHEEL),
          HID_LOGICAL_MIN  (0x81),
          HID_LOGICAL_MAX  (0x7F),
          HID_REPORT_COUNT (1),
          HID_REPORT_SIZE  (8),
          HID_INPUT        (HID_DATA | HID_VARIABLE | HID_RELATIVE),
        HID_USAGE_PAGE (HID_USAGE_PAGE_CONSUMER), 
          // Horizontal wheel scroll [-127, 127]
          HID_USAGE_N      (HID_USAGE_CONSUMER_AC_PAN, 2), 
          HID_LOGICAL_MIN  (0x81), 
          HID_LOGICAL_MAX  (0x7F), 
          HID_REPORT_COUNT (1), 
          HID_REPORT_SIZE  (8), 
          HID_INPUT        (HID_DATA | HID_VARIABLE | HID_RELATIVE), 
      HID_COLLECTION_END, 
    HID_COLLECTION_END,
    // gamepad
    HID_USAGE_PAGE (HID_USAGE_PAGE_DESKTOP),
    HID_USAGE      (HID_USAGE_DESKTOP_GAMEPAD),
    HID_COLLECTION (HID_COLLECTION_APPLICATION),
      HID_REPORT_ID  (GamePadReport::kID)
      // 16 bit Button Map
      HID_USAGE_PAGE (HID_USAGE_PAGE_BUTTON),
        HID_USAGE_MIN    (1),
        HID_USAGE_MAX    (16),
        HID_LOGICAL_MIN  (0),
        HID_LOGICAL_MAX  (1),
        HID_REPORT_COUNT (16),
        HID_REPORT_SIZE  (1),
        HID_INPUT        (HID_DATA | HID_VARIABLE | HID_ABSOLUTE),
      // X, Y, Z, Rz (min 0, max 255)
      HID_USAGE_PAGE (HID_USAGE_PAGE_DESKTOP),
        HID_USAGE        (HID_USAGE_DESKTOP_X),
        HID_USAGE        (HID_USAGE_DESKTOP_Y),
        HID_USAGE        (HID_USAGE_DESKTOP_Z),
        HID_USAGE        (HID_USAGE_DESKTOP_RZ),
        HID_LOGICAL_MIN  (0x00),
        HID_LOGICAL_MAX  (0xFF),
        HID_REPORT_COUNT (4),
        HID_REPORT_SIZE  (8),
        HID_INPUT        (HID_DATA | HID_VARIABLE | HID_ABSOLUTE),
    HID_COLLECTION_END,
    // consumer
    HID_USAGE_PAGE (HID_USAGE_PAGE_CONSUMER),
    HID_USAGE      (HID_USAGE_CONSUMER_CONTROL),
    HID_COLLECTION (HID_COLLECTION_APPLICATION),
      HID_REPORT_ID     (ConsumerReport::kID)
      HID_USAGE_MIN     (0x00),
      HID_USAGE_MAX_N   (0x0514, 2),
      HID_LOGICAL_MIN   (0x00),
      HID_LOGICAL_MAX_N (0x0514, 2),
      HID_REPORT_COUNT  (1),
      HID_REPORT_SIZE   (16),
      HID_INPUT         (HID_DATA | HID_ARRAY | HID_ABSOLUTE),
    HID_COLLECTION_END
};
// clang-format on

}  // namespace midi2usb

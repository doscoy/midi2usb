#pragma once

#include <type_traits>

namespace midi2usb {

template <class Type, std::size_t Size>
class RingBuffer {
  static_assert(Size > 1, "Size must be greater than 1");
  static_assert(Size != 0 && (Size & (Size - 1)) == 0, "Size must be pow2");

  static constexpr auto kIsScalar{std::is_scalar<Type>::value};
  using ArgType = typename std::conditional<kIsScalar, Type, const Type&>::type;
  using PeakRetType = ArgType;

 public:
  using ValueType = Type;
  static constexpr auto GetBufferSize() { return Size; }
  bool Available() const { return write_ != read_; }

  Type& WriteHeadRef() { return buffer_[write_]; }

  PeakRetType Peak() const { return buffer_[read_]; }

  void Clear() {
    write_ = 0;
    read_ = 0;
  }

  void IncWriteHead() {
    IncHelper(&write_);
    if (write_ == read_) IncHelper(&read_);
  }

  void IncReadHead() {
    if (write_ != read_) IncHelper(&read_);
  }

  void Push(ArgType item) {
    WriteHeadRef() = item;
    IncWriteHead();
  }

  Type Pop() {
    auto tmp{Peak()};
    IncReadHead();
    return tmp;
  }

 private:
  static void IncHelper(std::size_t* tgt) {
    constexpr auto kMask{Size - 1};
    *tgt = (*tgt + 1) & kMask;
  }

  Type buffer_[Size]{Type{}};
  std::size_t write_{0};
  std::size_t read_{0};
};

}  // namespace midi2usb

#pragma once

#ifdef MIDI2USBDEBUG

#define MIDI2USBDEBUGSETUP Serial.begin(115200);
#define MIDI2USBDEBUGPRINT(str) Serial.println(str);
#define MIDI2USBDEBUGPRINTF(fmt, ...) \
  Serial.printf(fmt, __VA_ARGS__);    \
  Serial.println();
#define MIDI2USBDEBUGPRINTREPORT(id, data, size)       \
  Serial.printf("%d: ", id);                           \
  {                                                    \
    auto ptr{reinterpret_cast<const uint8_t*>(data)};  \
    for (int i = 0; i < static_cast<int>(size); ++i) { \
      Serial.printf("%02x ", *ptr);                    \
      ++ptr;                                           \
    }                                                  \
  }                                                    \
  Serial.println();
#else

#define MIDI2USBDEBUGSETUP ((void)0);
#define MIDI2USBDEBUGPRINT(str) ((void)0);
#define MIDI2USBDEBUGPRINTF(fmt, ...) ((void)0);
#define MIDI2USBDEBUGPRINTREPORT(id, data, size) ((void)0);

#endif
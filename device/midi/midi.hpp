#pragma once

#include <MIDI.h>

#include "share/led.hpp"
#include "share/serialmidisetting.hpp"

namespace midi2usb {

class MidiMgr {
  using SerialPort = Uart;
  using SerialMidiPort = midi::SerialMIDI<Uart, SerialMidiPortSetting>;
  using SerialMidi = midi::MidiInterface<SerialMidiPort, SerialMidiSetting>;
  using SendLED = LEDFlasher<LED_BUILTIN, 100>;

 public:
  class MidiW;
  class MidiRW;

  MidiMgr(Uart* serialport)
      : serialport_{serialport},
        serialmidiport_{*serialport_},
        serialmidi_{serialmidiport_} {}

  void Init() {
    serialmidi_.begin(MIDI_CHANNEL_OMNI);
    serialmidi_.turnThruOff();
  }
  void UpdateLED() { led_.Update(); }
  bool Available() { return serialmidi_.read(); }

  MidiW GetWriter();
  MidiRW GetReadWriter();

 private:

  SerialPort* serialport_;
  SerialMidiPort serialmidiport_;
  SerialMidi serialmidi_;
  SendLED led_;
};

class MidiMgr::MidiW {
  friend class MidiMgr;

 public:
  auto Send(const uint8_t* data, size_t size) {
    const auto ret{serial_->write(data, size)};
    if (ret != 0) led_->Flash();
    return ret;
  }

 private:
  MidiW(MidiMgr* master) : serial_{master->serialport_}, led_{&master->led_} {}

  SerialPort* serial_;
  SendLED* led_;
};

class MidiMgr::MidiRW {
  friend class MidiMgr;

 public:
  auto GetType() const { return serialmidi_->getType(); }
  auto GetCh() const { return serialmidi_->getChannel(); }
  auto GetData1() const { return serialmidi_->getData1(); }
  auto GetData2() const { return serialmidi_->getData2(); }
  auto GetSysExData() const { return serialmidi_->getSysExArray(); }
  auto GetSysExSize() const { return serialmidi_->getSysExArrayLength(); }
  auto Send(const uint8_t* data, size_t size) {
    return writer_.Send(data, size);
  }

 private:
  MidiRW(MidiMgr* master)
      : writer_{master}, serialmidi_{&master->serialmidi_} {}

  MidiW writer_;
  SerialMidi* serialmidi_;
};

MidiMgr::MidiW MidiMgr::GetWriter() { return MidiW{this}; }
MidiMgr::MidiRW MidiMgr::GetReadWriter() { return MidiRW{this}; }

using MidiW = MidiMgr::MidiW;
using MidiRW = MidiMgr::MidiRW;

}  // namespace midi2usb
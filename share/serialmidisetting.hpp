#pragma once

namespace midi2usb {

struct SerialMidiPortSetting {
  // see midi::DefaultSerialSettings
  static constexpr long BaudRate{115200};
};

struct SerialMidiSetting {
  // see midi::DefaultSettings
  static constexpr bool UseRunningStatus{false};
  static constexpr bool HandleNullVelocityNoteOnAsNoteOff{true};
  static constexpr bool Use1ByteParsing{true};
  static constexpr unsigned SysExMaxSize{256};
  static constexpr bool UseSenderActiveSensing{false};
  static constexpr bool UseReceiverActiveSensing{false};
  static constexpr uint16_t SenderActiveSensingPeriodicity{0};
};

inline static constexpr uint8_t kLocalSysExID{0x7D};
enum LOCALSYSEXCMD : uint8_t { DISCONNECT, CONNECT, SENDCABLE, RECEIVECABLE };

// +----------------+----------------------------------------------------------+
// |     Byte       |                  Description                             |
// +----------------+----------------------------------------------------------+
// | F0,XX          | Local Exclusive Header  XX;kLocalSysExID                 |
// | 0000 0000 (xx) | xx;DISCONNECT                                            |
// | 1111 0111 (F7) | End of Exclusive (EOX)                                   |
// +----------------+----------------------------------------------------------+
// +----------------+----------------------------------------------------------+
// |     Byte       |                  Description                             |
// +----------------+----------------------------------------------------------+
// | F0,XX          | Local Exclusive Header  XX;kLocalSysExID                 |
// | 0000 0000 (xx) | xx;CONNECT                                               |
// | 0000 00gg (0g) | 0g;vid                                                   |
// | 0hhh hhhh (hh) | hh;vid   ->   vid = 0bgghh'hhhh'hiii'iiii                |
// | 0iii iiii (ii) | ii;vid                                                   |
// | 0000 00jj (0j) | 0j;pid                                                   |
// | 0kkk kkkk (kk) | kk;pid   ->   pid = 0bjjkk'kkkk'klll'llll                |
// | 0lll llll (ll) | ll;pid                                                   |
// | 1111 0111 (F7) | End of Exclusive (EOX)                                   |
// +----------------+----------------------------------------------------------+
// +----------------+----------------------------------------------------------+
// |     Byte       |                  Description                             |
// +----------------+----------------------------------------------------------+
// | F0,XX          | Local Exclusive Header  XX;kLocalSysExID                 |
// | 0000 0000 (xx) | xx;SENDCABLE                                             |
// | 0000 gggg (0g) | g;cable number                                           |
// | 1111 0111 (F7) | End of Exclusive (EOX)                                   |
// +----------------+----------------------------------------------------------+
// +----------------+----------------------------------------------------------+
// |     Byte       |                  Description                             |
// +----------------+----------------------------------------------------------+
// | F0,XX          | Local Exclusive Header  XX;kLocalSysExID                 |
// | 0000 0000 (xx) | xx;RECEIVECABLE                                          |
// | gggg hhhh (gh) | g;use filter(0 = not use, not 0 = use)                   |
// |                | h;cable number                                           |
// | 1111 0111 (F7) | End of Exclusive (EOX)                                   |
// +----------------+----------------------------------------------------------+

}
#pragma once

namespace midi2usb {

using MicroSec = decltype(micros());

template <MicroSec TgtDur>
class TimeKeeper {
 public:
  void Keep() {
    const MicroSec now{micros()};
    const MicroSec dur{time_ > now ? 0xFFFFFFFF - time_ + now : now - time_};
    if (dur < TgtDur) delayMicroseconds(TgtDur - dur);
    time_ = micros();
  }

 private:
  MicroSec time_{0};
};

}  // namespace midi2usb
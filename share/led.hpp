#pragma once

namespace midi2usb {

template <uint32_t Pin, unsigned Thr>
class LEDFlasher {
 public:
  void Flash() {
    digitalWrite(Pin, LOW);
    counter_ = Thr;
  }

  void Update() {
    if (counter_ > 0) {
      --counter_;
      if (counter_ == 0) {
        digitalWrite(Pin, HIGH);
      }
    }
  }

  void Hide() {
    counter_ = 0;
    digitalWrite(Pin, HIGH);
  }

 private:
  unsigned counter_;
};

}  // namespace midi2usb
#include <MIDI.h>
#include <usbh_midi.h>
#include <usbhub.h>

#include "share/led.hpp"
#include "share/serialmidisetting.hpp"
#include "share/timekeeper.hpp"

namespace midi2usb {

using MicroSec = decltype(micros());
using SerialMidiPort = midi::SerialMIDI<Uart, SerialMidiPortSetting>;
using SerialMidi = midi::MidiInterface<SerialMidiPort, SerialMidiSetting>;

static USBHost host;
static USBH_MIDI usbmidi{&host};
static SerialMidiPort serialmidiport(Serial1);
static SerialMidi serialmidi(serialmidiport);

static LEDFlasher<PIN_LED_TXL, 100> tx_led;
static LEDFlasher<PIN_LED_RXL, 100> rx_led;
static LEDFlasher<LED_BUILTIN, 100> uart_led;

static TimeKeeper<1000> timekeeper;

static struct Connect {
  bool enable{false};
  uint16_t vid{0};
  uint16_t pid{0};
  uint8_t sendcable{0};
  bool recvfilter{false};
  uint8_t recvcable{0};
} connect;

inline void Error();
inline void SendDisconnectSignal();
inline void SendConnectSignal();
inline void PollSerial();
inline void HandlingLocalSysex(const uint8_t* data, unsigned len);
inline void PollUSB();

}  // namespace midi2usb

void setup() {
  midi2usb::serialmidi.begin(MIDI_CHANNEL_OMNI);
  midi2usb::serialmidi.turnThruOff();

  if (midi2usb::host.Init()) {
    midi2usb::Error();
  }

  delay(500);
}

void loop() {
  midi2usb::timekeeper.Keep();

  midi2usb::tx_led.Update();
  midi2usb::rx_led.Update();
  midi2usb::uart_led.Update();

  midi2usb::host.Task();
  const auto state{midi2usb::host.getUsbTaskState()};

  if (midi2usb::connect.enable &&
      state == USB_DETACHED_SUBSTATE_WAIT_FOR_DEVICE) {
    midi2usb::connect = midi2usb::Connect{};
    midi2usb::SendDisconnectSignal();
  }

  if (state == USB_STATE_RUNNING) {
    midi2usb::connect.enable = true;

    if (midi2usb::usbmidi.idVendor() != midi2usb::connect.vid ||
        midi2usb::usbmidi.idProduct() != midi2usb::connect.pid) {
      midi2usb::connect.vid = midi2usb::usbmidi.idVendor();
      midi2usb::connect.pid = midi2usb::usbmidi.idProduct();
      midi2usb::SendConnectSignal();
    }

    midi2usb::PollSerial();
    midi2usb::PollUSB();
  }
}

namespace midi2usb {

void Error() {
  while (true) {  // halt
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(PIN_LED2, HIGH);
    digitalWrite(PIN_LED3, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(PIN_LED2, LOW);
    digitalWrite(PIN_LED3, LOW);
    delay(1000);
  }
}

void SendDisconnectSignal() {
  const uint8_t msg[]{0xF0, kLocalSysExID, LOCALSYSEXCMD::DISCONNECT, 0xF7};
  serialmidi.sendSysEx(sizeof(msg), msg, true);
  uart_led.Flash();
}

void SendConnectSignal() {
  const uint8_t msg[]{0xF0,
                      kLocalSysExID,
                      LOCALSYSEXCMD::CONNECT,
                      (connect.vid >> 14) & 0x7F,
                      (connect.vid >> 7) & 0x7F,
                      connect.vid & 0x7F,
                      (connect.pid >> 14) & 0x7F,
                      (connect.pid >> 7) & 0x7F,
                      connect.pid & 0x7F,
                      0xF7};
  serialmidi.sendSysEx(sizeof(msg), msg, true);
  uart_led.Flash();
}

void PollSerial() {
  while (serialmidi.read()) {
    if (serialmidi.getType() == midi::MidiType::SystemExclusive) {
      const auto data{serialmidi.getSysExArray()};
      const auto len{serialmidi.getSysExArrayLength()};
      if (data[1] == kLocalSysExID) {
        HandlingLocalSysex(data, len);

      } else {
        usbmidi.SendSysEx((uint8_t*)data, len, connect.sendcable);
      }

    } else {
      const uint8_t head{serialmidi.getChannel() == 0
                             ? serialmidi.getType()
                             : serialmidi.getType() |
                                   (serialmidi.getChannel() - 1)};
      uint8_t buf[]{head, serialmidi.getData1(), serialmidi.getData2()};
      usbmidi.SendData(buf, connect.sendcable);
    }

    tx_led.Flash();
  }
}

void HandlingLocalSysex(const uint8_t* data, unsigned len) {
  switch (static_cast<LOCALSYSEXCMD>(data[2])) {
    case LOCALSYSEXCMD::SENDCABLE:
      connect.sendcable = data[3] & 0x0F;
      break;
    case LOCALSYSEXCMD::RECEIVECABLE:
      connect.recvfilter = (data[3] & 0xF0) != 0;
      connect.recvcable = (data[3] << 4) & 0xF0;
      break;
    default:
      break;
  }
}

void PollUSB() {
  uint8_t buf[4];
  uint8_t size;
  while (size = usbmidi.RecvData(buf, true), size > 0) {
    rx_led.Flash();

    // filtering cable
    if (connect.recvfilter && connect.recvcable != (*buf & 0xF0)) continue;
    
    Serial1.write(buf + 1, size);
    uart_led.Flash();
  }
}

}  // namespace midi2usb

# midi2usb

usb midiデバイスをusb hidデバイス(keyborad, mouse, gamepad, consumer)として接続する

## 回路図

```
           +--------------------------------+
           |                                |
           |                          +-----------+
           |                          |     |     |
           |           ---------------|--   |   --|---------------
    usb    |    usb    |  v g 3 1 9 8 7 |   |   | 6 5 4 3 2 1 0  |
-------    |    -------|  c n v 0       |   |   |                |-------
  vbus|----+----|vbus  |  c d 3         |   |   | seeeduino xiao |      |
    D+|---------|D+    |                |   |   |    (device)    |      |
    D-|---------|D-    | seeeduino xiao |   |   |                |      |
   gnd|----+----|gnd   |     (host)     |   |   |         3 g v  |      |
-------    |    -------|                |   |   |       1 v n c  |-------
  ->       |           |  0 1 2 3 4 5 6 |   |   | 7 8 9 0 3 d c  |  -> 
from midi  |           ---------------|--   |   --|---------|-|--- to pc
           |                          |     |     |         | |
           |                          +-----------+         | |
           |                                |               | |
           +------------------------------------------------+ |
                                            |                 |
                                            +-----------------+
```

## 使用ライブラリ
 - [Arduino MIDI Library](https://github.com/FortySevenEffects/arduino_midi_library)
 - [Adafruit TinyUSB Library](https://github.com/adafruit/Adafruit_TinyUSB_Arduino)
   - seeeduino xiaoの都合でversion0.10.5を使用(新しいものでは動作せず)
 - [USB Host Library SAMD](https://github.com/gdsports/USB_Host_Library_SAMD)

## キーバインド

[テーブル](./device/app/state/common/table.hpp)参照

## 参考

 - http://kato-h.cocolog-nifty.com/khweblog/2021/11/post-9033f1.html